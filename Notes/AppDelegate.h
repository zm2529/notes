//
//  AppDelegate.h
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

@class NoteFolderListViewController;
@class NoteListViewController;
@class NoteViewController;
@class NoteInformationViewController;
@class NoteEditHistoryViewController;
@class NoteEditHistoryDetailViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@property (nonatomic, strong) NoteFolderListViewController *folderListVC;
@property (nonatomic, strong) NoteListViewController *noteListVC;
@property (nonatomic, strong) NoteViewController *noteVC;
@property (nonatomic, strong) NoteInformationViewController *noteInformationVC;
@property (nonatomic, strong) NoteEditHistoryViewController *historyVC;
@property (nonatomic, strong) NoteEditHistoryDetailViewController *historyDetailVC;


@end

