//
//  AppDelegate.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "AppDelegate.h"

#import "ToolsManager.h"

#import "NoteFolderListViewController.h"

#import "NoteDataManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    NoteFolderListViewController *noteFolderListVC = [[NoteFolderListViewController alloc] init];
    
    UINavigationController *naVC = [[UINavigationController alloc] initWithRootViewController:noteFolderListVC];
    naVC.navigationBar.tintColor = Orange_Color;

    [naVC.navigationBar setBackgroundImage:[ToolsManager imageWithColor:White_Color
                                                                andSize:CGSizeMake(SCREEN_WIDTH, 64.f)]
                             forBarMetrics:UIBarMetricsDefault];
//    naVC.navigationBar.alpha = 0.1f;
//    naVC.navigationBar.translucent = NO;
//    naVC.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:Orange_Color}];
    
    self.window.rootViewController = naVC;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_APPLICATION_WILL_RESIGNACTIVE
                                                        object:nil];
    [[NoteDataManager sharedManager] saveAll];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_APPLICATION_DID_ENTER_BACKGROUND
                                                        object:nil];
    [[NoteDataManager sharedManager] saveAll];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_APPLICATION_WILL_TERMINATE
                                                        object:nil];
    [[NoteDataManager sharedManager] saveAll];
}

@end
