//
//  ScreenAdaptationHelper.m
//  v6cn-iPhone_lib
//
//  Created by zm on 15/8/24.
//  Copyright (c) 2015年 Darcy Niu. All rights reserved.
//

#import "ScreenAdaptationHelper.h"

#import "ToolsManager.h"

#define ADAPTATION_PARAMETER (SCREEN_WIDTH / 320.f)

#define NULL_FLOAT -999999

//EqualSpaceLayout 使用
typedef NS_ENUM(NSInteger, EnumEqualSpaceLayoutType)
{
    EnumEqualSpaceLayoutTypeVertical = 0,
    EnumEqualSpaceLayoutTypeHorizontal = 1
};

@implementation ScreenAdaptationHelper


#pragma mark - 只改变大小
+ (CGSize)adaptationWithSuperViewSize:(CGSize)superViewSize
                      percentForWidth:(CGFloat)widthPercent
                     percentForHeight:(CGFloat)heightPercent
{
    CGFloat width = scaleFloor(widthPercent * superViewSize.width);
    CGFloat height = scaleFloor(heightPercent * superViewSize.height);
    
    
    return CGSizeMake(width, height);
}

+ (CGSize)adaptationWithSuperViewSize:(CGSize)superViewSize
                      percentForWidth:(CGFloat)widthPercent
                               height:(CGFloat)height
{
    CGFloat width = scaleFloor(widthPercent * superViewSize.width);
    
    return CGSizeMake(width, height);
}

+ (CGSize)adaptationWithSuperViewSize:(CGSize)superViewSize
                      percentForWidth:(CGFloat)widthPercent
                          aspectRatio:(CGFloat)aspectRatio
{
    CGFloat width = scaleFloor(widthPercent * superViewSize.width);
    
    CGFloat height = scaleFloor(width / aspectRatio);
    
    return CGSizeMake(width, height);
}


//设置 frame 高是 比例
+ (void)adaptationWithView:(UIView *)view
           percentForWidth:(CGFloat)widthPercent
          percentForHeight:(CGFloat)heightPercent
             superViewSize:(CGSize)superViewSize
{
    CGSize tempSize = [ScreenAdaptationHelper adaptationWithSuperViewSize:superViewSize
                                                          percentForWidth:widthPercent
                                                         percentForHeight:heightPercent];
    view.frame = (CGRect){
        .origin = view.frame.origin,
        .size = tempSize,
    };
}

//高不是比例
+ (void)adaptationWithView:(UIView *)view
           percentForWidth:(CGFloat)widthPercent
                    height:(CGFloat)height
             superViewSize:(CGSize)superViewSize
{
    CGSize tempSize = [ScreenAdaptationHelper adaptationWithSuperViewSize:superViewSize
                                                          percentForWidth:widthPercent
                                                                   height:height];
    
    view.frame = (CGRect){
        .origin = view.frame.origin,
        .size = tempSize,
    };
}

+ (void)adaptationWithView:(UIView *)view
           percentForWidth:(CGFloat)widthPercent
               aspectRatio:(CGFloat)aspectRatio
             superViewSize:(CGSize)superViewSize
{
    CGSize tempSize = [ScreenAdaptationHelper adaptationWithSuperViewSize:superViewSize
                                                          percentForWidth:widthPercent
                                                              aspectRatio:aspectRatio];
    
    view.frame = (CGRect){
        .origin = view.frame.origin,
        .size = tempSize,
    };
}

//+ (CGRect)getSuperViewFrame:(UIView *)view
//{
//    return view.superview ? view.superview.frame : SCREEN_RECT;
//}































#pragma mark - 只改变位置

#pragma mark - --LocationLayout
+ (void)locationLayoutByPercentWithAlignment:(EnumLocationLayoutAlignment)aligment
                                        view:(UIView *)view
                               offsetPercent:(CGPoint)offsetPercent
                               superViewSize:(CGSize)superViewSize
{
    view.frame = [ScreenAdaptationHelper locationLayoutByPercentWithAlignment:aligment
                                                                         size:view.frame.size
                                                                offsetPercent:offsetPercent
                                                                superViewSize:superViewSize];
}

+ (CGRect)locationLayoutByPercentWithAlignment:(EnumLocationLayoutAlignment)aligment
                                          size:(CGSize)size
                                 offsetPercent:(CGPoint)offsetPercent
                                 superViewSize:(CGSize)superViewSize
{
    CGFloat tempoffsetX = (offsetPercent.x * superViewSize.width);
    CGFloat tempoffsetY = (offsetPercent.y * superViewSize.height);
    
    return [ScreenAdaptationHelper locationLayoutWithAlignment:aligment
                                                          size:size
                                                        offset:CGPointMake(tempoffsetX, tempoffsetY)
                                                 superViewSize:superViewSize];
}


+ (void)locationLayoutWithAlignment:(EnumLocationLayoutAlignment)aligment
                               view:(UIView *)view
                             offset:(CGPoint)offset
                      superViewSize:(CGSize)superViewSize
{
    view.frame = [ScreenAdaptationHelper locationLayoutWithAlignment:aligment
                                                                size:view.frame.size
                                                              offset:offset
                                                       superViewSize:superViewSize];
}

+ (CGRect)locationLayoutWithAlignment:(EnumLocationLayoutAlignment)aligment
                                 size:(CGSize)size
                               offset:(CGPoint)offset
                        superViewSize:(CGSize)superViewSize
{
    CGFloat x = offset.x;
    CGFloat y = offset.y;
    CGFloat width = size.width;
    CGFloat height = size.height;
    
    CGSize contentSize = superViewSize;
    
    switch (aligment) {
        case EnumLocationLayoutAlignmentTop: {
            
            x = (contentSize.width - width) / 2 + offset.x;
            y = offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentLeft: {
            
            x = offset.x;
            y = (contentSize.height - height) / 2 + offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentTopLeft: {
            
            x = offset.x;
            y = offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentBottom: {
            
            x = (contentSize.width - width) / 2 + offset.x;
            y = (contentSize.height - height) + offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentLeftBottom: {
            
            x = offset.x;
            y = (contentSize.height - height) + offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentRight: {
            
            x = (contentSize.width - width) + offset.x;
            y = (contentSize.height - height) / 2 + offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentBottomRight: {
            
            x = (contentSize.width - width) + offset.x;
            y = (contentSize.height - height) + offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentRightTop: {
            
            x = (contentSize.width - width) + offset.x;
            y = offset.y;
            
            break;
        }
        case EnumLocationLayoutAlignmentCenter: {
            
            x = (contentSize.width - width) / 2 + offset.x;
            y = (contentSize.height - height) / 2 + offset.y;
            
            break;
        }
        default: {
            break;
        }
    }
    
    return CGRectMake(scaleFloor(x), scaleFloor(y), scaleFloor(width), scaleFloor(height));
}















#pragma mark - --EqualSpaceLayout

#pragma mark - ----EqualSpaceLayout 等间隔布局 自动设置frame
//垂直等间隔布局 （各个view的height需相同）（自动设置frame）
+ (void)verticalEqualSpaceLayoutWithViews:(NSArray *)arrViews
              equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                            superViewSize:(CGSize)superViewSize
                                 topSpace:(CGFloat)topSpace
                              bottomSpace:(CGFloat)bottomSpace
                                viewSpace:(CGFloat)viewSpace
                          percentForSpace:(CGFloat)spacePercent
{
    [ScreenAdaptationHelper equalSpaceLayoutWithViews:arrViews
                                 equalSpaceLayoutType:EnumEqualSpaceLayoutTypeVertical
                          equalSpaceLayoutContentMode:contentMode
                                        superViewSize:superViewSize
                                           edgeInsets:UIEdgeInsetsMake(topSpace, 0, bottomSpace, 0)
                                            viewSpace:viewSpace
                                      percentForSpace:spacePercent];
}


// 水平等间隔布局 （各个view的width需相同）（自动设置frame）
+ (void)horizontalEqualSpaceLayoutWithViews:(NSArray *)arrViews
                equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                              superViewSize:(CGSize)superViewSize
                                  leftSpace:(CGFloat)leftSpace
                                 rightSpace:(CGFloat)rightSpace
                                  viewSpace:(CGFloat)viewSpace
                            percentForSpace:(CGFloat)spacePercent
{
    [ScreenAdaptationHelper equalSpaceLayoutWithViews:arrViews
                                 equalSpaceLayoutType:EnumEqualSpaceLayoutTypeHorizontal
                          equalSpaceLayoutContentMode:contentMode
                                        superViewSize:superViewSize
                                           edgeInsets:UIEdgeInsetsMake(0, leftSpace, 0, rightSpace)
                                            viewSpace:viewSpace
                                      percentForSpace:spacePercent];
}


#pragma mark - ----EqualSpaceLayout 少参数方法
/*
 contentMode = EnumEqualSpaceLayoutContentModeFill
 
 起始位置 top 间隔viewSpace或spacePercent 排列
 */
+ (void)verticalKeepSideWithViews:(NSArray *)arrViews
                         topSpace:(CGFloat)topSpace
                        viewSpace:(CGFloat)viewSpace
                  percentForSpace:(CGFloat)spacePercent
{
    [ScreenAdaptationHelper verticalEqualSpaceLayoutWithViews:arrViews
                                  equalSpaceLayoutContentMode:EnumEqualSpaceLayoutContentModeFill
                                                superViewSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT)
                                                     topSpace:topSpace
                                                  bottomSpace:0
                                                    viewSpace:viewSpace
                                              percentForSpace:spacePercent];
}


+ (void)horizontalKeepSideWithViews:(NSArray *)arrViews
                          leftSpace:(CGFloat)leftSpace
                          viewSpace:(CGFloat)viewSpace
                    percentForSpace:(CGFloat)spacePercent
{
    [ScreenAdaptationHelper horizontalEqualSpaceLayoutWithViews:arrViews
                                    equalSpaceLayoutContentMode:EnumEqualSpaceLayoutContentModeFill
                                                  superViewSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT)
                                                      leftSpace:leftSpace
                                                     rightSpace:0
                                                      viewSpace:viewSpace
                                                percentForSpace:spacePercent];
}


/*
 contentMode = EnumEqualSpaceLayoutContentModeFill
 
 充满 top 和 bottom 之间
 */
+ (void)verticalFillWithViews:(NSArray *)arrViews
                superViewSize:(CGSize)superViewSize
                     topSpace:(CGFloat)topSpace
                  bottomSpace:(CGFloat)bottomSpace
{
    [ScreenAdaptationHelper verticalEqualSpaceLayoutWithViews:arrViews
                                  equalSpaceLayoutContentMode:EnumEqualSpaceLayoutContentModeFill
                                                superViewSize:superViewSize
                                                     topSpace:topSpace
                                                  bottomSpace:bottomSpace
                                                    viewSpace:0
                                              percentForSpace:0];
}

+ (void)horizontalFillWithViews:(NSArray *)arrViews
                  superViewSize:(CGSize)superViewSize
                      leftSpace:(CGFloat)leftSpace
                     rightSpace:(CGFloat)rightSpace
{
    [ScreenAdaptationHelper horizontalEqualSpaceLayoutWithViews:arrViews
                                    equalSpaceLayoutContentMode:EnumEqualSpaceLayoutContentModeFill
                                                  superViewSize:superViewSize
                                                      leftSpace:leftSpace
                                                     rightSpace:rightSpace
                                                      viewSpace:0
                                                percentForSpace:0];
}

/*
 自动设置间隔
 */
+ (void)verticalAutoSpaceWithViews:(NSArray *)arrViews
       equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                     superViewSize:(CGSize)superViewSize
                          topSpace:(CGFloat)topSpace
                       bottomSpace:(CGFloat)bottomSpace
{
    [ScreenAdaptationHelper verticalEqualSpaceLayoutWithViews:arrViews
                                  equalSpaceLayoutContentMode:contentMode
                                                superViewSize:superViewSize
                                                     topSpace:topSpace
                                                  bottomSpace:bottomSpace
                                                    viewSpace:0
                                              percentForSpace:0];
}

+ (void)horizontalAutoSpaceWithViews:(NSArray *)arrViews
         equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                       superViewSize:(CGSize)superViewSize
                           leftSpace:(CGFloat)leftSpace
                          rightSpace:(CGFloat)rightSpace
{
    [ScreenAdaptationHelper horizontalEqualSpaceLayoutWithViews:arrViews
                                    equalSpaceLayoutContentMode:contentMode
                                                  superViewSize:superViewSize
                                                      leftSpace:leftSpace
                                                     rightSpace:rightSpace
                                                      viewSpace:0
                                                percentForSpace:0];
}



#pragma mark - ----EqualSpaceLayout 返回值方法
//垂直等间隔布局 （各个view的height需相同） 返回每个view的frame
+ (NSArray *)verticalEqualSpaceLayoutWithViewFrame:(CGRect)frame
                                         viewCount:(NSInteger)viewCount
                       equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                                     superViewSize:(CGSize)superViewSize
                                          topSpace:(CGFloat)topSpace
                                       bottomSpace:(CGFloat)bottomSpace
                                         viewSpace:(CGFloat)viewSpace
                                   percentForSpace:(CGFloat)spacePercent
{
    return [ScreenAdaptationHelper equalSpaceLayoutWithViewFrame:frame
                                                       viewCount:viewCount
                                            equalSpaceLayoutType:EnumEqualSpaceLayoutTypeVertical
                                     equalSpaceLayoutContentMode:contentMode
                                                   superViewSize:superViewSize
                                                       viewSpace:viewSpace
                                                      edgeInsets:UIEdgeInsetsMake(topSpace, 0, bottomSpace, 0)
                                                 percentForSpace:spacePercent];
}


// 水平等间隔布局 （各个view的width需相同） 返回每个view的frame
+ (NSArray *)horizontalEqualSpaceLayoutWithViewFrame:(CGRect)frame
                                           viewCount:(NSInteger)viewCount
                         equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                                       superViewSize:(CGSize)superViewSize
                                           leftSpace:(CGFloat)leftSpace
                                          rightSpace:(CGFloat)rightSpace
                                           viewSpace:(CGFloat)viewSpace
                                     percentForSpace:(CGFloat)spacePercent
{
    return [ScreenAdaptationHelper equalSpaceLayoutWithViewFrame:frame
                                                       viewCount:viewCount
                                            equalSpaceLayoutType:EnumEqualSpaceLayoutTypeHorizontal
                                     equalSpaceLayoutContentMode:contentMode
                                                   superViewSize:superViewSize
                                                       viewSpace:viewSpace
                                                      edgeInsets:UIEdgeInsetsMake(0, leftSpace, 0, rightSpace)
                                                 percentForSpace:spacePercent];
}


#pragma mark - ----EqualSpaceLayout 自动设置指定view 高度 或 宽度
+ (NSArray *)equalSpaceLayoutWithViewFrame:(CGRect)frame
                                 viewCount:(NSInteger)viewCount
                      equalSpaceLayoutType:(EnumEqualSpaceLayoutType)layoutType
               equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                             superViewSize:(CGSize)superViewSize
                                 viewSpace:(CGFloat)viewSpace
                                edgeInsets:(UIEdgeInsets)contentInsets
                           percentForSpace:(CGFloat)spacePercent
{
    NSMutableArray *arrViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < viewCount; i++) {
        
        UIView *view = [[UIView alloc] initWithFrame:frame];
        
        [arrViews addObject:view];
    }
    
    [ScreenAdaptationHelper equalSpaceLayoutWithViews:arrViews
                                 equalSpaceLayoutType:layoutType
                          equalSpaceLayoutContentMode:contentMode
                                        superViewSize:superViewSize
                                           edgeInsets:contentInsets
                                            viewSpace:viewSpace
                                      percentForSpace:spacePercent];
    
    NSMutableArray *arrRect = [[NSMutableArray alloc] init];
    for (UIView *tempView in arrViews) {
        [arrRect addObject:[NSValue valueWithCGRect:tempView.frame]];
    }
    return arrRect;
}



+ (void)equalSpaceLayoutWithViews:(NSArray *)arrViews
             equalSpaceLayoutType:(EnumEqualSpaceLayoutType)layoutType
      equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                    superViewSize:(CGSize)superViewSize
                       edgeInsets:(UIEdgeInsets)contentInsets
                        viewSpace:(CGFloat)viewSpace
                  percentForSpace:(CGFloat)spacePercent
{
    NSInteger nAllCount = arrViews.count;
    CGFloat space, allViewAndSpaceLength;
    
    CGFloat currentViewMaxPosition = NULL_FLOAT;
    
    CGSize contentSize = CGSizeMake(superViewSize.width - contentInsets.left - contentInsets.right,
                                    superViewSize.height - contentInsets.top - contentInsets.bottom);
    
    if (nAllCount > 0) {
        
        
        switch (layoutType) {
            case EnumEqualSpaceLayoutTypeVertical: {
                
                CGFloat allViewLength = 0;//全部 view 的长度
                allViewAndSpaceLength = 0;//全部 view 和 n － 1个 间隔的长度
                
                for (UIView *tempView in arrViews) {
                    allViewLength += CGRectGetHeight(tempView.frame);
                }
                
                if (spacePercent != 0) {
                    space = (spacePercent * superViewSize.height);
                }
                else{//spacePercent == 0 使用viewSpace
                    if (viewSpace != 0) {
                        space = viewSpace;
                    }
                    else{//viewSpace == 0 使用 自动计算间隔
                        
                        if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
                            space = ((contentSize.height - allViewLength) / (nAllCount + 1));
                        }
                        else{
                            
                            if (nAllCount > 1) {
                                space = ((contentSize.height - allViewLength) / (nAllCount - 1));
                            }
                            else{//只有一个view
                                space = (contentSize.height - allViewLength);
                            }
                        }
                        
                    }
                }
                
                allViewAndSpaceLength = allViewLength + space * (nAllCount - 1);
                
                NSDictionary *dicLayoutInfo = @{
                                                @"contentSize" : [NSValue valueWithCGSize:contentSize],
                                                @"allViewAndSpaceLength" : [NSNumber numberWithFloat:allViewAndSpaceLength],
                                                @"space" : [NSNumber numberWithFloat:space],
                                                @"contentInsets" : [NSValue valueWithUIEdgeInsets:contentInsets],
                                                @"currentViewMaxPosition" : [NSNumber numberWithFloat:currentViewMaxPosition],
                                                };
                
                for (int i = 0; i < nAllCount; i++) {
                    UIView *tempView = arrViews[i];
                    
                    currentViewMaxPosition = [ScreenAdaptationHelper layoutWithView:tempView
                                                             currentViewMaxPosition:currentViewMaxPosition
                                                                         layoutInfo:dicLayoutInfo
                                                                         layoutType:EnumEqualSpaceLayoutTypeVertical
                                                                        contentMode:contentMode];
                }
                
                break;
            }
            case EnumEqualSpaceLayoutTypeHorizontal: {
                
                CGFloat allViewLength = 0;
                allViewAndSpaceLength = 0;
                
                for (UIView *tempView in arrViews) {
                    allViewLength += CGRectGetWidth(tempView.frame);
                }
                
                if (spacePercent != 0) {
                    space = (spacePercent * superViewSize.width);
                }
                else{//spacePercent == 0 使用viewSpace
                    if (viewSpace != 0) {
                        space = viewSpace;
                    }
                    else{//viewSpace == 0 使用 自动计算间隔
                        
                        if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
                            space = ((contentSize.width - allViewLength) / (nAllCount + 1));
                        }
                        else{
                            
                            if (nAllCount > 1) {
                                space = ((contentSize.width - allViewLength) / (nAllCount - 1));
                            }
                            else{//只有一个view
                                space = (contentSize.width - allViewLength);
                            }
                        }
                        
                    }
                }
                
                allViewAndSpaceLength = (allViewLength + space * (nAllCount - 1));
                
                NSDictionary *dicLayoutInfo = @{
                                                @"contentSize" : [NSValue valueWithCGSize:contentSize],
                                                @"allViewAndSpaceLength" : [NSNumber numberWithFloat:allViewAndSpaceLength],
                                                @"space" : [NSNumber numberWithFloat:space],
                                                @"contentInsets" : [NSValue valueWithUIEdgeInsets:contentInsets],
                                                @"currentViewMaxPosition" : [NSNumber numberWithFloat:currentViewMaxPosition],
                                                };
                
                for (int i = 0; i < nAllCount; i++) {
                    UIView *tempView = arrViews[i];
                    
                    currentViewMaxPosition = [ScreenAdaptationHelper layoutWithView:tempView
                                                             currentViewMaxPosition:currentViewMaxPosition
                                                                         layoutInfo:dicLayoutInfo
                                                                         layoutType:EnumEqualSpaceLayoutTypeHorizontal
                                                                        contentMode:contentMode];
                }
                
                break;
            }
            default: {
                break;
            }
        }
        
    }
}



//设置view的位置
+ (CGFloat)layoutWithView:(UIView *)view
   currentViewMaxPosition:(CGFloat)currentViewMaxPosition
               layoutInfo:(NSDictionary *)layoutInfo
               layoutType:(EnumEqualSpaceLayoutType)layoutType
              contentMode:(EnumEqualSpaceLayoutContentMode)contentMode
{
    
    CGFloat x = CGRectGetMinX(view.frame);
    CGFloat y = CGRectGetMinY(view.frame);
    CGFloat width = CGRectGetWidth(view.frame);
    CGFloat height = CGRectGetHeight(view.frame);
    
    
    //    @{
    //      @"contentSize" : [NSValue valueWithCGSize:contentSize],
    //      @"allViewAndSpaceLength" : [NSNumber numberWithFloat:allViewAndSpaceLength],
    //      @"space" : [NSNumber numberWithFloat:space],
    //      @"contentInsets" : [NSValue valueWithUIEdgeInsets:contentInsets],
    //      @"currentViewMaxPosition" : [NSNumber numberWithFloat:currentViewMaxPosition],
    //      }
    
    CGSize contentSize = [[layoutInfo objectForKey:@"contentSize"] CGSizeValue];
    
    CGFloat allViewAndSpaceLength = [[layoutInfo objectForKey:@"allViewAndSpaceLength"] floatValue];//全部view 和 间隔 的长度
    CGFloat space = [[layoutInfo objectForKey:@"space"] floatValue];//view之间的间隔
    
    UIEdgeInsets contentInsets = [[layoutInfo objectForKey:@"contentInsets"] UIEdgeInsetsValue];//偏移
    
    CGFloat length = 0;//一个view的长度
    
    switch (layoutType) {
        case EnumEqualSpaceLayoutTypeVertical: {//垂直
            
            length = CGRectGetHeight(view.frame);
            
            switch (contentMode) {
                case EnumEqualSpaceLayoutContentModeCenter: {
                    
                    if (currentViewMaxPosition == NULL_FLOAT) {
                        currentViewMaxPosition = ((contentSize.height - allViewAndSpaceLength) / 2 + contentInsets.top);
                    }
                    y = currentViewMaxPosition;
                    
                    break;
                }
                case EnumEqualSpaceLayoutContentModeFill: {
                    if (currentViewMaxPosition == NULL_FLOAT) {
                        currentViewMaxPosition = contentInsets.top;
                    }
                    y = currentViewMaxPosition;
                    
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case EnumEqualSpaceLayoutTypeHorizontal: {//水平
            
            length = CGRectGetWidth(view.frame);
            
            switch (contentMode) {
                case EnumEqualSpaceLayoutContentModeCenter: {
                    
                    if (currentViewMaxPosition == NULL_FLOAT) {
                        currentViewMaxPosition = ((contentSize.width - allViewAndSpaceLength) / 2 + contentInsets.left);
                    }
                    x = currentViewMaxPosition;
                    break;
                }
                case EnumEqualSpaceLayoutContentModeFill: {
                    if (currentViewMaxPosition == NULL_FLOAT) {
                        currentViewMaxPosition = contentInsets.left;
                    }
                    x = currentViewMaxPosition;
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        default: {
            break;
        }
    }
    
    view.frame = CGRectMake(scaleFloor(x), scaleFloor(y), scaleFloor(width), scaleFloor(height));
    
    currentViewMaxPosition += length + space;
    
    return currentViewMaxPosition;
}



//垂直等间隔布局 自动计算指定view的高
+ (void)verticalEqualSpaceLayoutWithViews:(NSArray *)arrViews
                autoAdaptationHeightIndex:(NSInteger)autoAdaptionIndex
                            superViewSize:(CGSize)superViewSize
                                 topSpace:(CGFloat)topSpace
                              bottomSpace:(CGFloat)bottomSpace
                                viewSpace:(CGFloat)viewSpace
                          percentForSpace:(CGFloat)spacePercent
{
    NSInteger nAllCount = arrViews.count;
    
    CGFloat contentHeight = superViewSize.height - topSpace - bottomSpace;
    
    CGFloat allViewLength = 0, allSpaceLength = 0;
    
    CGFloat space = 0;
    if (spacePercent != 0) {
        space = (spacePercent * contentHeight);
    }
    else{
        space = viewSpace;
    }
    allSpaceLength = (space * (nAllCount - 1));
    
    
    for (NSInteger i = 0; i < nAllCount; i++) {
        if (i != autoAdaptionIndex) {
            UIView *tempView = arrViews[i];
            allViewLength += CGRectGetHeight(tempView.frame);
        }
    }
    
    CGFloat autoViewLength = (contentHeight - allSpaceLength - allViewLength);
    
    
    CGFloat currentViewMaxX = topSpace;
    
    for (NSInteger i = 0; i < nAllCount; i++) {
        
        CGFloat length = 0;
        
        
        UIView *tempView = arrViews[i];
        
        if (i != autoAdaptionIndex) {
            length = CGRectGetHeight(tempView.frame);
        }
        else{
            length = autoViewLength;
            
            [ScreenAdaptationHelper changeViewFrame:tempView
                                           dicFrame:@{
                                                      @"h" : [NSNumber numberWithFloat:length]
                                                      }];
        }
        
        [ScreenAdaptationHelper changeViewFrame:tempView
                                       dicFrame:@{
                                                  @"y" : [NSNumber numberWithFloat:currentViewMaxX]
                                                  }];
        
        currentViewMaxX += length + space;
        
    }
}



//水平等间隔布局 自动计算指定view的宽
+ (void)horizontalEqualSpaceLayoutWithViews:(NSArray *)arrViews
                   autoAdaptationWidthIndex:(NSInteger)autoAdaptionIndex
                              superViewSize:(CGSize)superViewSize
                                  leftSpace:(CGFloat)leftSpace
                                 rightSpace:(CGFloat)rightSpace
                                  viewSpace:(CGFloat)viewSpace
                            percentForSpace:(CGFloat)spacePercent
{
    NSInteger nAllCount = arrViews.count;
    
    CGFloat contentWidth = superViewSize.width - leftSpace - rightSpace;
    
    CGFloat allViewLength = 0, allSpaceLength = 0;
    
    CGFloat space = 0;
    if (spacePercent != 0) {
        space = (spacePercent * contentWidth);
    }
    else{
        space = viewSpace;
    }
    allSpaceLength = (space * (nAllCount - 1));
    
    
    for (NSInteger i = 0; i < nAllCount; i++) {
        if (i != autoAdaptionIndex) {
            UIView *tempView = arrViews[i];
            allViewLength += CGRectGetWidth(tempView.frame);
        }
    }
    
    CGFloat autoViewLength = (contentWidth - allSpaceLength - allViewLength);
    
    
    CGFloat currentViewMaxX = leftSpace;
    
    for (NSInteger i = 0; i < nAllCount; i++) {
        
        CGFloat length = 0;
        
        
        UIView *tempView = arrViews[i];
        
        if (i != autoAdaptionIndex) {
            length = CGRectGetWidth(tempView.frame);
        }
        else{
            length = autoViewLength;
            
            [ScreenAdaptationHelper changeViewFrame:tempView
                                           dicFrame:@{
                                                      @"w" : [NSNumber numberWithFloat:length]
                                                      }];
        }
        
        [ScreenAdaptationHelper changeViewFrame:tempView
                                       dicFrame:@{
                                                  @"x" : [NSNumber numberWithFloat:currentViewMaxX]
                                                  }];
        
        currentViewMaxX += length + space;
        
    }
}


//改变view的frame
+ (void)changeViewFrame:(UIView *)view
               dicFrame:(NSDictionary *)dicFrame
{
    CGRect frame = view.frame;
    
    if ([dicFrame objectForKey:@"x"]) {
        frame.origin.x = scaleFloor([[dicFrame objectForKey:@"x"] floatValue]);
    }
    
    if ([dicFrame objectForKey:@"y"]) {
        frame.origin.y = scaleFloor([[dicFrame objectForKey:@"y"] floatValue]);
    }
    
    if ([dicFrame objectForKey:@"w"]) {
        frame.size.width = scaleFloor([[dicFrame objectForKey:@"w"] floatValue]);
    }
    
    if ([dicFrame objectForKey:@"h"]) {
        frame.size.height = scaleFloor([[dicFrame objectForKey:@"h"] floatValue]);
    }
    
    view.frame = frame;
}


/**
 *  @author zm, 15-09-06 09:09:27
 *
 *  平面 布局 （水平 和  竖直 两个方向）
 *
 *  @param arrViews             需要布局的View
 *  @param superViewSize        superview的 size
 *  @param contentMode          填充模式
 *  @param contentInset         填充范围
 *  @param horizontalSpace      水平 方向 view的间隔
 *  @param verticalSpace        竖直 方向 view的间隔
 *  @param isPriorHrizoontal    是否优先水平方向布局
 */
+ (void)planarSpaceLayoutWithViews:(NSArray *)arrViews
                     superViewSize:(CGSize)superViewSize
                       contentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                     contentInsets:(UIEdgeInsets)contentInset
                   horizontalSpace:(CGFloat)horizontalSpace
                     verticalSpace:(CGFloat)verticalSpace
             priorHorizontalLayout:(BOOL)isPriorHrizoontal
                         floatType:(EnumPlanarSpaceLayoutFloatType)floatType
{
    [ScreenAdaptationHelper planarSpaceLayout:arrViews
                                superViewSize:superViewSize
                                  contentMode:contentMode
                                contentInsets:contentInset
                              horizontalSpace:horizontalSpace
                                verticalSpace:verticalSpace
                        layoutHorizontalSpace:horizontalSpace
                          layoutVerticalSpace:verticalSpace
                        priorHorizontalLayout:isPriorHrizoontal
                                    floatType:floatType];
}

/**
 *  @author zm, 15-09-06 10:09:00
 *
 *  平面 布局 （水平 和  竖直 两个方向） 可设置 最小水平间隔 和 最小竖直间隔
 *
 *  @param arrViews           需要布局的View
 *  @param superViewSize      superview的 size
 *  @param contentMode        填充模式
 *  @param contentInset       填充范围
 *  @param minHorizontalSpace 最小水平间隔
 *  @param minVerticalSpace   最小竖直间隔
 *  @param isPriorHrizoontal  是否优先水平方向布局
 */
+ (void)planarSpaceLayoutMinSpaceWithViews:(NSArray *)arrViews
                             superViewSize:(CGSize)superViewSize
                               contentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                             contentInsets:(UIEdgeInsets)contentInset
                        minHorizontalSpace:(CGFloat)minHorizontalSpace
                          minVerticalSpace:(CGFloat)minVerticalSpace
                     priorHorizontalLayout:(BOOL)isPriorHrizoontal
                                 floatType:(EnumPlanarSpaceLayoutFloatType)floatType
{
    [ScreenAdaptationHelper planarSpaceLayout:arrViews
                                superViewSize:superViewSize
                                  contentMode:contentMode
                                contentInsets:contentInset
                              horizontalSpace:minHorizontalSpace
                                verticalSpace:minVerticalSpace
                        layoutHorizontalSpace:0.f
                          layoutVerticalSpace:0.f
                        priorHorizontalLayout:isPriorHrizoontal
                                    floatType:floatType];
}

//平面布局
+ (void)planarSpaceLayout:(NSArray *)arrViews
            superViewSize:(CGSize)superViewSize
              contentMode:(EnumEqualSpaceLayoutContentMode)contentMode
            contentInsets:(UIEdgeInsets)contentInset
          horizontalSpace:(CGFloat)horizontalSpace
            verticalSpace:(CGFloat)verticalSpace
    layoutHorizontalSpace:(CGFloat)layoutHorizontalSpace
      layoutVerticalSpace:(CGFloat)layoutVerticalSpace
    priorHorizontalLayout:(BOOL)isPriorHrizoontal
                floatType:(EnumPlanarSpaceLayoutFloatType)floatType
{
    CGSize contentSize = CGSizeMake(superViewSize.width - contentInset.left - contentInset.right,
                                    superViewSize.height - contentInset.top - contentInset.bottom);
    
    
    CGFloat lineWidth = 0;
    CGFloat lineHeight = 0;
    
    NSInteger lastLayoutLineViewsCount = 0;
    
    if (isPriorHrizoontal) {//优先 水平 布局
        
        CGFloat nextLineOrigin = contentInset.top;
        
        
        NSMutableArray *arrLayoutViews;
        
        
        NSInteger allCount = arrViews.count;
        for (NSInteger i = 0; i < allCount; i++) {
            
            UIView *tempView = arrViews[i];
            
            if (lineWidth == 0) {//新的一行
                
                arrLayoutViews = [[NSMutableArray alloc] init];
                
                if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
                    lineWidth += horizontalSpace;
                }
            }
            
            lineWidth += CGRectGetWidth(tempView.frame);
            
            CGFloat tempLineWidth = lineWidth;
            if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
                tempLineWidth += horizontalSpace;
            }
            
            if (tempLineWidth <= contentSize.width) {//当前这一行放得下
                
                //设置view origin 的值
                tempView.frame = [ScreenAdaptationHelper locationLayoutWithAlignment:EnumLocationLayoutAlignmentTopLeft
                                                                                size:tempView.frame.size
                                                                              offset:CGPointMake(0, nextLineOrigin)
                                                                       superViewSize:contentSize];
                
                
                
                //获取 当前行中 最大的高度
                lineHeight = MAX(lineHeight, CGRectGetHeight(tempView.frame));
                
                
                //加入 水平布局 数组
                [arrLayoutViews addObject:tempView];
                
                
                lineWidth += horizontalSpace;
                
                if (i >= allCount - 1) {//已到最后一个数据
                    
                    if (lastLayoutLineViewsCount == 0) {
                        lastLayoutLineViewsCount = [ScreenAdaptationHelper calculateLineViewsCount:tempView
                                                                                    andContentSize:contentSize
                                                                                   horizontalSpace:horizontalSpace
                                                                                     verticalSpace:verticalSpace
                                                                                        layoutType:EnumEqualSpaceLayoutTypeHorizontal contentMode:contentMode];
                    }
                    
                    //float left
                    if (floatType == EnumPlanarSpaceLayoutFloatTypeLeft) {
                        for (NSInteger j = arrLayoutViews.count; j < lastLayoutLineViewsCount ; j++) {
                            UIView *occupyPlaceView = [[UIView alloc] initWithFrame:tempView.frame];
                            
                            [arrLayoutViews addObject:occupyPlaceView];
                        }
                    }
                    
                    //float right
                    if (floatType == EnumPlanarSpaceLayoutFloatTypeRight) {
                        for (NSInteger j = arrLayoutViews.count; j < lastLayoutLineViewsCount ; j++) {
                            UIView *occupyPlaceView = [[UIView alloc] initWithFrame:tempView.frame];
                            
                            [arrLayoutViews insertObject:occupyPlaceView atIndex:0];
                        }
                    }
                    
                    [ScreenAdaptationHelper horizontalEqualSpaceLayoutWithViews:arrLayoutViews
                                                    equalSpaceLayoutContentMode:contentMode
                                                                  superViewSize:superViewSize
                                                                      leftSpace:contentInset.left
                                                                     rightSpace:contentInset.right
                                                                      viewSpace:layoutHorizontalSpace
                                                                percentForSpace:0];
                }
                
            }
            else{ //一行 已经放满
                
                lastLayoutLineViewsCount = arrLayoutViews.count;
                
                //水平 布局
                [ScreenAdaptationHelper horizontalEqualSpaceLayoutWithViews:arrLayoutViews
                                                equalSpaceLayoutContentMode:contentMode
                                                              superViewSize:superViewSize
                                                                  leftSpace:contentInset.left
                                                                 rightSpace:contentInset.right
                                                                  viewSpace:layoutHorizontalSpace
                                                            percentForSpace:0];
                
                nextLineOrigin += lineHeight + verticalSpace;
                
                lineHeight = 0;
                lineWidth = 0;
                
                i--;
            }
            
        }
    }
    else{//优先 竖直 布局
        CGFloat nextLineOrigin = contentInset.left;
        
        
        NSMutableArray *arrLayoutViews;
        
        
        NSInteger allCount = arrViews.count;
        for (NSInteger i = 0; i < allCount; i++) {
            
            UIView *tempView = arrViews[i];
            
            if (lineHeight == 0) {//新的一行
                
                arrLayoutViews = [[NSMutableArray alloc] init];
                
                if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
                    lineHeight += verticalSpace;
                }
            }
            
            lineHeight += CGRectGetHeight(tempView.frame);
            
            CGFloat tempLineHeight = lineHeight;
            if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
                tempLineHeight += verticalSpace;
            }
            
            if (tempLineHeight <= contentSize.height) {//当前这一列放得下
                
                //设置view origin 的值
                tempView.frame = [ScreenAdaptationHelper locationLayoutWithAlignment:EnumLocationLayoutAlignmentTopLeft
                                                                                size:tempView.frame.size
                                                                              offset:CGPointMake(nextLineOrigin, 0)
                                                                       superViewSize:contentSize];
                
                
                
                //获取 当前列中 最大的宽度
                lineWidth = MAX(lineWidth, CGRectGetWidth(tempView.frame));
                
                
                //加入 竖直布局 数组
                [arrLayoutViews addObject:tempView];
                
                
                lineHeight += verticalSpace;
                
                if (i >= allCount - 1) {//已到最后一个数据
                    
                    if (lastLayoutLineViewsCount == 0) {
                        lastLayoutLineViewsCount = [ScreenAdaptationHelper calculateLineViewsCount:tempView
                                                                                    andContentSize:contentSize
                                                                                   horizontalSpace:horizontalSpace
                                                                                     verticalSpace:verticalSpace
                                                                                        layoutType:EnumEqualSpaceLayoutTypeVertical contentMode:contentMode];
                    }
                    
                    //float left
                    if (floatType == EnumPlanarSpaceLayoutFloatTypeTop) {
                        for (NSInteger j = arrLayoutViews.count; j < lastLayoutLineViewsCount ; j++) {
                            UIView *occupyPlaceView = [[UIView alloc] initWithFrame:tempView.frame];
                            
                            [arrLayoutViews addObject:occupyPlaceView];
                        }
                    }
                    
                    //float right
                    if (floatType == EnumPlanarSpaceLayoutFloatTypeBottom) {
                        for (NSInteger j = arrLayoutViews.count; j < lastLayoutLineViewsCount ; j++) {
                            UIView *occupyPlaceView = [[UIView alloc] initWithFrame:tempView.frame];
                            
                            [arrLayoutViews insertObject:occupyPlaceView atIndex:0];
                        }
                    }

                    [ScreenAdaptationHelper verticalEqualSpaceLayoutWithViews:arrLayoutViews
                                                  equalSpaceLayoutContentMode:contentMode
                                                                superViewSize:superViewSize
                                                                     topSpace:contentInset.top
                                                                  bottomSpace:contentInset.bottom
                                                                    viewSpace:layoutVerticalSpace
                                                              percentForSpace:0];
                }
                
            }
            else{ //一列 已经放满
                //竖直 布局
                [ScreenAdaptationHelper verticalEqualSpaceLayoutWithViews:arrLayoutViews
                                              equalSpaceLayoutContentMode:contentMode
                                                            superViewSize:superViewSize
                                                                 topSpace:contentInset.top
                                                              bottomSpace:contentInset.bottom
                                                                viewSpace:layoutVerticalSpace
                                                          percentForSpace:0];
                
                nextLineOrigin += lineWidth + horizontalSpace;
                
                lineHeight = 0;
                lineWidth = 0;
                
                i--;
            }
        }
    }
}

+ (NSInteger)calculateLineViewsCount:(UIView *)lastViews
                      andContentSize:(CGSize)contentSize
                     horizontalSpace:(CGFloat)horizontalSpace
                       verticalSpace:(CGFloat)verticalSpace
                          layoutType:(EnumEqualSpaceLayoutType)layoutType
                         contentMode:(EnumEqualSpaceLayoutContentMode)contentMode
{
    CGFloat lineViewsCount = 0;
    CGFloat lineLength = 0;
    
    if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
        switch (layoutType) {
            case EnumEqualSpaceLayoutTypeVertical: {
                lineLength += verticalSpace;
                break;
            }
            case EnumEqualSpaceLayoutTypeHorizontal: {
                lineLength += horizontalSpace;
                break;
            }
            default: {
                break;
            }
        }
    }
    
    while (YES) {
        
        switch (layoutType) {
            case EnumEqualSpaceLayoutTypeVertical: {
                lineLength += CGRectGetHeight(lastViews.frame);
                break;
            }
            case EnumEqualSpaceLayoutTypeHorizontal: {
                lineLength += CGRectGetWidth(lastViews.frame);
                break;
            }
            default: {
                break;
            }
        }
        
        CGFloat tempLineLength = lineLength;
        if (contentMode == EnumEqualSpaceLayoutContentModeCenter) {
            switch (layoutType) {
                case EnumEqualSpaceLayoutTypeVertical: {
                    tempLineLength += verticalSpace;
                    break;
                }
                case EnumEqualSpaceLayoutTypeHorizontal: {
                    tempLineLength += horizontalSpace;
                    break;
                }
                default: {
                    break;
                }
            }
        }
        
        if (tempLineLength <= contentSize.width){
            lineViewsCount++;
            switch (layoutType) {
                case EnumEqualSpaceLayoutTypeVertical: {
                    lineLength += verticalSpace;
                    break;
                }
                case EnumEqualSpaceLayoutTypeHorizontal: {
                    lineLength += horizontalSpace;
                    break;
                }
                default: {
                    break;
                }
            }
        }
        else{
            return lineViewsCount;
        }
    }
}















#pragma mark - 混合方法 （定位 和 适应大小）

// 需要 宽高比 （宽 ／ 高）
+ (CGRect)locationAndSizeWithAlignment:(EnumLocationLayoutAlignment)aligment
                       percentForWidth:(CGFloat)widthPercent
                           aspectRatio:(CGFloat)aspectRatio
                                offset:(CGPoint)offset
                         superViewSize:(CGSize)superViewSize
{
    return [ScreenAdaptationHelper locationLayoutWithAlignment:aligment
                                                          size:[ScreenAdaptationHelper
                                                                adaptationWithSuperViewSize:superViewSize
                                                                percentForWidth:widthPercent
                                                                aspectRatio:aspectRatio]
                                                        offset:offset
                                                 superViewSize:superViewSize];
}

// 需要 高 的 确定值
+ (CGRect)locationAndSizeWithAlignment:(EnumLocationLayoutAlignment)aligment
                       percentForWidth:(CGFloat)widthPercent
                                height:(CGFloat)height
                                offset:(CGPoint)offset
                         superViewSize:(CGSize)superViewSize
{
    return [ScreenAdaptationHelper locationLayoutWithAlignment:aligment
                                                          size:[ScreenAdaptationHelper
                                                                adaptationWithSuperViewSize:superViewSize
                                                                percentForWidth:widthPercent
                                                                height:height]
                                                        offset:offset
                                                 superViewSize:superViewSize];
}


@end


#pragma mark - zoom
CGRect originAdaptation(CGFloat x, CGFloat y, CGFloat width, CGFloat height)
{
    CGFloat tempX = x > 0 ? (x * ADAPTATION_PARAMETER) : (x / ADAPTATION_PARAMETER);
    CGFloat tempY = y > 0 ? (y * ADAPTATION_PARAMETER) : (y / ADAPTATION_PARAMETER);
    
    return CGRectMake(scaleFloor(tempX), scaleFloor(tempY), scaleFloor(width), scaleFloor(height));
}

CGRect rectAdaptation(CGFloat x, CGFloat y, CGFloat width, CGFloat height)
{
    CGFloat tempX = x > 0 ? (x * ADAPTATION_PARAMETER) : (x / ADAPTATION_PARAMETER);
    CGFloat tempY = y > 0 ? (y * ADAPTATION_PARAMETER) : (y / ADAPTATION_PARAMETER);
    
    CGFloat tempWidth = width == 320 ? SCREEN_WIDTH : (width * ADAPTATION_PARAMETER);
    CGFloat tempHeight = (height * ADAPTATION_PARAMETER);
    
    return CGRectMake(scaleFloor(tempX), scaleFloor(tempY), scaleFloor(tempWidth), scaleFloor(tempHeight));
    
}

CGFloat lengthAdaptation(CGFloat length)
{
    if (length > 0) {
        return scaleFloor(length * ADAPTATION_PARAMETER);
    }
    else{
        return scaleFloor(length / ADAPTATION_PARAMETER);
    }
}

CGFloat scaleFloor(CGFloat lenght)
{
    if ([UIScreen mainScreen].scale == 2.f) {
        return floor(lenght * 2.f) / 2.f;
    }
    else{
        if ([UIScreen mainScreen].scale == 3.f) {
            return floor(lenght * 3.f) / 3.f;
        }
    }
    return floor(lenght);
}
