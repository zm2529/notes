//
//  ScreenAdaptationHelper.h
//  v6cn-iPhone_lib
//
//  Created by zm on 15/8/24.
//  Copyright (c) 2015年 Darcy Niu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScreenAdaptationHelper : NSObject


#pragma mark - 只改变大小 （根据屏幕 适应 大小）
//+ (CGRect)rectAdaptaionWithSuperViewFrame:(CGRect)superViewFrame
//                            numerialFrame:(CGRect)numerialFrame
//                             percentFrame:(CGRect)percentFrame;

/**
 *  @author zm, 15-08-26 15:08:31
 *
 *  按屏幕 适配 view大小 （宽高 都是比例）
 *
 *  @param superViewSize  superview的Size
 *  @param widthPercent   宽 占 superview宽的百分比
 *  @param heightPercent  高 占 superview高的百分比
 *
 *  @return CGSize
 */
+ (CGSize)adaptationWithSuperViewSize:(CGSize)superViewSize
                      percentForWidth:(CGFloat)widthPercent
                     percentForHeight:(CGFloat)heightPercent;

/**
 *  @author zm, 15-08-26 15:08:35
 *
 *  按屏幕 适配 view大小 （宽是 比例）
 *
 *  @param superViewSize  superview的Size
 *  @param widthPercent   宽 占 superview宽的百分比
 *  @param height         高的具体数值
 *
 *  @return CGSize
 */
+ (CGSize)adaptationWithSuperViewSize:(CGSize)superViewSize
                      percentForWidth:(CGFloat)widthPercent
                               height:(CGFloat)height;

/**
 *  @author zm, 15-08-26 15:08:35
 *
 *  按屏幕 适配 view大小 （宽是 比例）
 *
 *  @param superViewSize  superview的Size
 *  @param widthPercent   宽 占 superview宽的百分比
 *  @param aspectRatio    宽高比 （宽 ／ 高）
 *
 *  @return CGSize
 */
+ (CGSize)adaptationWithSuperViewSize:(CGSize)superViewSize
                      percentForWidth:(CGFloat)widthPercent
                          aspectRatio:(CGFloat)aspectRatio;

/**
 *  @author zm, 15-08-26 15:08:50
 *
 *  按屏幕 适配 view大小 自动设置view.frame （宽高 都是比例）
 *
 *  @param view             需要适配的view
 *  @param widthPercent     宽 占superview宽的百分比
 *  @param heightPercent    高 占superview高的百分比
 *  @param superViewSize    superview的Size
 */
+ (void)adaptationWithView:(UIView *)view
           percentForWidth:(CGFloat)widthPercent
          percentForHeight:(CGFloat)heightPercent
             superViewSize:(CGSize)superViewSize;

/**
 *  @author zm, 15-08-26 15:08:36
 *
 *  按屏幕 适配 view大小 自动设置view.frame （宽 是比例）
 *
 *  @param view             需要适配的view
 *  @param widthPercent     宽 占superview宽的百分比
 *  @param height           高的具体数值
 *  @param superViewSize    superview的Size
 */
+ (void)adaptationWithView:(UIView *)view
           percentForWidth:(CGFloat)widthPercent
                    height:(CGFloat)height
             superViewSize:(CGSize)superViewSize;


/**
 *  @author zm, 15-08-26 15:08:36
 *
 *  按屏幕 适配 view大小 自动设置view.frame （宽 是比例）
 *
 *  @param view             需要适配的view
 *  @param widthPercent     宽 占superview宽的百分比
 *  @param aspectRatio      宽高比（宽 ／ 高）
 *  @param superViewSize    superview的Size
 */
+ (void)adaptationWithView:(UIView *)view
           percentForWidth:(CGFloat)widthPercent
               aspectRatio:(CGFloat)aspectRatio
             superViewSize:(CGSize)superViewSize;























#pragma mark - 只改变位置 (定位 和 布局)
typedef NS_ENUM(NSInteger, EnumLocationLayoutAlignment)//布局 对齐方式
{
    EnumLocationLayoutAlignmentTop = 0,
    EnumLocationLayoutAlignmentLeft,
    EnumLocationLayoutAlignmentTopLeft,
    EnumLocationLayoutAlignmentBottom,
    EnumLocationLayoutAlignmentLeftBottom,
    EnumLocationLayoutAlignmentRight,
    EnumLocationLayoutAlignmentBottomRight,
    EnumLocationLayoutAlignmentRightTop,
    EnumLocationLayoutAlignmentCenter,
};

typedef NS_ENUM(NSInteger, EnumEqualSpaceLayoutContentMode)
{
    EnumEqualSpaceLayoutContentModeCenter,
    EnumEqualSpaceLayoutContentModeFill, //
};

//平面布局 float 类型
typedef NS_ENUM(NSInteger, EnumPlanarSpaceLayoutFloatType)
{
    EnumPlanarSpaceLayoutFloatTypeTop,
    EnumPlanarSpaceLayoutFloatTypeLeft,
    EnumPlanarSpaceLayoutFloatTypeBottom,
    EnumPlanarSpaceLayoutFloatTypeRight,
    EnumPlanarSpaceLayoutFloatTypeNull,
};



#pragma mark - --LocationLayout

/**
 *  @author zm, 15-08-25 18:08:11
 *
 *  位置布局 size
 *
 *  @param aligment         对齐方式
 *  @param size             view大小
 *  @param offset           偏移 （屏幕坐标系）
 *  @param superViewSize    superview的Size
 *
 *  @return 布局后 view 的 frame
 */
+ (CGRect)locationLayoutWithAlignment:(EnumLocationLayoutAlignment)aligment
                                 size:(CGSize)size
                               offset:(CGPoint)offset
                        superViewSize:(CGSize)superViewSize;

/**
 *  @author zm, 15-08-26 15:08:07
 *
 *  位置布局 （自动设置frame）
 *
 *  @param aligment         对齐方式
 *  @param view             需要布局的view
 *  @param offset           偏移（屏幕坐标系）
 *  @param superViewSize    superview的Size
 */
+ (void)locationLayoutWithAlignment:(EnumLocationLayoutAlignment)aligment
                               view:(UIView *)view
                             offset:(CGPoint)offset
                      superViewSize:(CGSize)superViewSize;
/**
 *  @author zm, 15-08-26 15:08:24
 *
 *  位置布局 使用 百分比 （自动设置frame）
 *
 *  @param aligment         对齐方式
 *  @param view             需要布局的view
 *  @param offsetPercent    偏移（屏幕坐标系）
 *  @param superViewSize    superview的Size
 */
+ (void)locationLayoutByPercentWithAlignment:(EnumLocationLayoutAlignment)aligment
                                        view:(UIView *)view
                               offsetPercent:(CGPoint)offsetPercent
                               superViewSize:(CGSize)superViewSize;

/**
 *  @author zm, 15-08-25 18:08:11
 *
 *  位置布局 size 使用 百分比
 *
 *  @param aligment         对齐方式
 *  @param size             view大小
 *  @param offset           偏移 （屏幕坐标系）
 *  @param superViewSize    superview的Size
 *
 *  @return 布局后 view 的 frame
 */
+ (CGRect)locationLayoutByPercentWithAlignment:(EnumLocationLayoutAlignment)aligment
                                          size:(CGSize)size
                                 offsetPercent:(CGPoint)offsetPercent
                                 superViewSize:(CGSize)superViewSize;







#pragma mark - --EqualSpaceLayout

#pragma mark - ----EqualSpaceLayout 等间隔布局 自动设置frame
/**
 *  @author zm, 15-08-26 15:08:57
 *
 *  垂直等间隔布局 （自动设置frame）
 *
 *  @param arrViews         需要布局的view
 *  @param contentMode      填充方式
 *  @param superViewSize    superviewSize
 *  @param topSpace         superview 顶边 到 第一个view的间距
 *  @param bottomSpace      superview 底边 到 最后一个view的间距
 *  @param viewSpace        view之间的间距 (spacePercent == 0时有效）（传0时 自动计算间隔)
 *  @param spacePercent     view之间的间距 占 superview的百分比
 */
+ (void)verticalEqualSpaceLayoutWithViews:(NSArray *)arrViews
              equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                            superViewSize:(CGSize)superViewSize
                                 topSpace:(CGFloat)topSpace
                              bottomSpace:(CGFloat)bottomSpace
                                viewSpace:(CGFloat)viewSpace
                          percentForSpace:(CGFloat)spacePercent;

/**
 *  @author zm, 15-08-26 15:08:57
 *
 *  水平等间隔布局 （自动设置frame）
 *
 *  @param arrViews         需要布局的view
 *  @param contentMode      填充方式
 *  @param superViewSize    superview 的 size
 *  @param leftSpace        superview 左边 到 第一个view的间距
 *  @param rightSpace       superview 右边 到 最后一个view的间距
 *  @param viewSpace        view之间的间距 (spacePercent == 0时有效）（传0时 自动计算间隔)
 *  @param spacePercent     view之间的间距 占 superview的百分比
 */
+ (void)horizontalEqualSpaceLayoutWithViews:(NSArray *)arrViews
                equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                              superViewSize:(CGSize)superViewSize
                                  leftSpace:(CGFloat)leftSpace
                                 rightSpace:(CGFloat)rightSpace
                                  viewSpace:(CGFloat)viewSpace
                            percentForSpace:(CGFloat)spacePercent;



#pragma mark - ----EqualSpaceLayout 少参数方法
/*
 contentMode = EnumEqualSpaceLayoutContentModeFill
 
 起始位置 top 间隔viewSpace或spacePercent 排列
 */
+ (void)verticalKeepSideWithViews:(NSArray *)arrViews
                         topSpace:(CGFloat)topSpace
                        viewSpace:(CGFloat)viewSpace
                  percentForSpace:(CGFloat)spacePercent;


+ (void)horizontalKeepSideWithViews:(NSArray *)arrViews
                          leftSpace:(CGFloat)leftSpace
                          viewSpace:(CGFloat)viewSpace
                    percentForSpace:(CGFloat)spacePercent;


/*
 contentMode = EnumEqualSpaceLayoutContentModeFill
 
 充满 top 和 bottom 之间
 */
+ (void)verticalFillWithViews:(NSArray *)arrViews
                superViewSize:(CGSize)superViewSize
                     topSpace:(CGFloat)topSpace
                  bottomSpace:(CGFloat)bottomSpace;

+ (void)horizontalFillWithViews:(NSArray *)arrViews
                  superViewSize:(CGSize)superViewSize
                      leftSpace:(CGFloat)leftSpace
                     rightSpace:(CGFloat)rightSpace;

/*
 自动设置间隔
 */
+ (void)verticalAutoSpaceWithViews:(NSArray *)arrViews
       equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                     superViewSize:(CGSize)superViewSize
                          topSpace:(CGFloat)topSpace
                       bottomSpace:(CGFloat)bottomSpace;

+ (void)horizontalAutoSpaceWithViews:(NSArray *)arrViews
         equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                       superViewSize:(CGSize)superViewSize
                           leftSpace:(CGFloat)leftSpace
                          rightSpace:(CGFloat)rightSpace;




#pragma mark - ----EqualSpaceLayout 返回值方法
/**
 *  @author zm, 15-08-26 15:08:17
 *
 *  垂直等间隔布局  返回每个view的frame （每个view的 高 需相同）
 *
 *  @param frame            其中一个view的frame
 *  @param viewCount        需布局view的个数
 *  @param contentMode      填充方式
 *  @param superViewSize    superview
 *  @param topSpace         superview 顶边 到 第一个view的间距
 *  @param bottomSpace      superview 底边 到 最后一个view的间距
 *  @param viewSpace        view之间的间距 (spacePercent == 0时有效）（传0时 自动计算间隔)
 *  @param spacePercent     view之间的间距 占 superview的百分比
 *
 *  @return 各个view的Frame
 */
+ (NSArray *)verticalEqualSpaceLayoutWithViewFrame:(CGRect)frame
                                         viewCount:(NSInteger)viewCount
                       equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                                     superViewSize:(CGSize)superViewSize
                                          topSpace:(CGFloat)topSpace
                                       bottomSpace:(CGFloat)bottomSpace
                                         viewSpace:(CGFloat)viewSpace
                                   percentForSpace:(CGFloat)spacePercent;
/**
 *  @author zm, 15-08-26 15:08:17
 *
 *  水平等间隔布局 返回每个view的frame （每个view的 宽 需相同）
 *
 *  @param frame            其中一个view的frame
 *  @param viewCount        需布局view的个数
 *  @param contentMode      填充方式
 *  @param superViewSize    superview
 *  @param topSpace         superview 左边 到 第一个view的间距
 *  @param bottomSpace      superview 右边 到 最后一个view的间距
 *  @param viewSpace        view之间的间距 (spacePercent == 0时有效）（传0时 自动计算间隔)
 *  @param spacePercent     view之间的间距 占 superview的百分比
 *
 *  @return 各个view的Frame
 */
+ (NSArray *)horizontalEqualSpaceLayoutWithViewFrame:(CGRect)frame
                                           viewCount:(NSInteger)viewCount
                         equalSpaceLayoutContentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                                       superViewSize:(CGSize)superViewSize
                                           leftSpace:(CGFloat)leftSpace
                                          rightSpace:(CGFloat)rightSpace
                                           viewSpace:(CGFloat)viewSpace
                                     percentForSpace:(CGFloat)spacePercent;



#pragma mark - ----EqualSpaceLayout 自动设置指定view 高度 或 宽度
/**
 *  @author zm, 15-08-28 10:08:30
 *
 *  垂直等间隔布局 自动计算高度 自动设置frame
 *
 *  @param arrViews          需要布局的view
 *  @param autoAdaptionIndex 需要自动计算高度的 序号
 *  @param superViewSize     superview 的 size
 *  @param topSpace          superview 上边 到 第一个view的间距
 *  @param bottomSpace       superview 下边 到 最后一个view的间距
 *  @param viewSpace         view之间的间距 (spacePercent == 0时有效）
 *  @param spacePercent      view之间的间距 占 superview的百分比
 */
+ (void)verticalEqualSpaceLayoutWithViews:(NSArray *)arrViews
                autoAdaptationHeightIndex:(NSInteger)autoAdaptionIndex
                            superViewSize:(CGSize)superViewSize
                                 topSpace:(CGFloat)topSpace
                              bottomSpace:(CGFloat)bottomSpace
                                viewSpace:(CGFloat)viewSpace
                          percentForSpace:(CGFloat)spacePercent;

/**
 *  @author zm, 15-08-28 10:08:30
 *
 *  水平等间隔布局 自动计算宽度 自动设置frame
 *
 *  @param arrViews          需要布局的view
 *  @param autoAdaptionIndex 需要自动计算宽度的 序号
 *  @param superViewSize     superview 的 size
 *  @param leftSpace         superview 左边 到 第一个view的间距
 *  @param rightSpace        superview 右边 到 最后一个view的间距
 *  @param viewSpace         view之间的间距 (spacePercent == 0时有效）
 *  @param spacePercent      view之间的间距 占 superview的百分比
 */
+ (void)horizontalEqualSpaceLayoutWithViews:(NSArray *)arrViews
                   autoAdaptationWidthIndex:(NSInteger)autoAdaptionIndex
                              superViewSize:(CGSize)superViewSize
                                  leftSpace:(CGFloat)leftSpace
                                 rightSpace:(CGFloat)rightSpace
                                  viewSpace:(CGFloat)viewSpace
                            percentForSpace:(CGFloat)spacePercent;


/**
 *  @author zm, 15-09-06 09:09:27
 *
 *  平面 布局 （水平 和  竖直 两个方向）
 *
 *  @param arrViews             需要布局的View
 *  @param superViewSize        superview的 size
 *  @param contentMode          填充模式
 *  @param contentInset         填充范围
 *  @param horizontalSpace      水平 方向 view的间隔
 *  @param verticalSpace        竖直 方向 view的间隔
 *  @param isPriorHrizoontal    是否优先水平方向布局
 */
+ (void)planarSpaceLayoutWithViews:(NSArray *)arrViews
                     superViewSize:(CGSize)superViewSize
                       contentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                     contentInsets:(UIEdgeInsets)contentInset
                   horizontalSpace:(CGFloat)horizontalSpace
                     verticalSpace:(CGFloat)verticalSpace
             priorHorizontalLayout:(BOOL)isPriorHrizoontal
                         floatType:(EnumPlanarSpaceLayoutFloatType)floatType;

/**
 *  @author zm, 15-09-06 10:09:00
 *
 *  平面 布局 （水平 和  竖直 两个方向） 可设置 最小水平间隔 和 最小竖直间隔
 *
 *  @param arrViews           需要布局的View
 *  @param superViewSize      superview的 size
 *  @param contentMode        填充模式
 *  @param contentInset       填充范围
 *  @param minHorizontalSpace 最小水平间隔
 *  @param minVerticalSpace   最小竖直间隔
 *  @param isPriorHrizoontal  是否优先水平方向布局
 */
+ (void)planarSpaceLayoutMinSpaceWithViews:(NSArray *)arrViews
                             superViewSize:(CGSize)superViewSize
                               contentMode:(EnumEqualSpaceLayoutContentMode)contentMode
                             contentInsets:(UIEdgeInsets)contentInset
                        minHorizontalSpace:(CGFloat)minHorizontalSpace
                          minVerticalSpace:(CGFloat)minVerticalSpace
                     priorHorizontalLayout:(BOOL)isPriorHrizoontal
                                 floatType:(EnumPlanarSpaceLayoutFloatType)floatType;





















#pragma mark - 混合方法 （定位 和 适应大小）

// 需要 宽高比 （宽 ／ 高）
+ (CGRect)locationAndSizeWithAlignment:(EnumLocationLayoutAlignment)aligment
                       percentForWidth:(CGFloat)widthPercent
                           aspectRatio:(CGFloat)aspectRatio
                                offset:(CGPoint)offset
                         superViewSize:(CGSize)superViewSize;

// 需要 高 的 确定值
+ (CGRect)locationAndSizeWithAlignment:(EnumLocationLayoutAlignment)aligment
                       percentForWidth:(CGFloat)widthPercent
                                height:(CGFloat)height
                                offset:(CGPoint)offset
                         superViewSize:(CGSize)superViewSize;

@end

#pragma mark -

/**
 *  @author zm, 15-08-25 18:08:36
 *
 *  !!!只针对5的设计图有效  按屏幕 适配 view 位置x，y
 *
 *  @param x
 *  @param y
 *  @param width
 *  @param height
 *
 *  @return CGRect
 */
CGRect originAdaptation(CGFloat x, CGFloat y, CGFloat width, CGFloat height);

/**
 *  @author zm, 15-08-26 09:08:37
 *
 *  !!!只针对5的设计图有效 按屏幕 适配 view frame
 *
 *  @param x      x
 *  @param y      y
 *  @param width  width
 *  @param height height
 *
 *  @return CGRect
 */
CGRect rectAdaptation(CGFloat x, CGFloat y, CGFloat width, CGFloat height);

/**
 *  @author zm, 15-08-25 18:08:24
 *
 *  !!!只针对5的设计图有效 按屏幕 适配 长度
 *
 *  @param length
 *
 *  @return CGFloat
 */
CGFloat lengthAdaptation(CGFloat length);

//根据scale向下取整
CGFloat scaleFloor(CGFloat lenght);

