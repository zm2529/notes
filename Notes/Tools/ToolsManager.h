//
//  ToolsManager.h
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>
/******************************** NOTIFICATION *************************************/


/**
 *  @author zm, 16-02-16 16:08:08
 *
 *  恢复记录 通知
 *
 *  @param @{@"noteText" : NSAttributedString, @"noteDate" : NSString, @"noteInfoID" : NSString,}
 */
#define NOTIFICATION_NOTE_RECOVERY @"NOTIFICATION_NOTE_RECOVERY"

/**
 *  @author zm, 16-02-17 16:51:31
 *
 *  修改标题
 *
 *  @param @{@"noteTitle" : NSString, @"noteInfoID" : NSString,}
 */
#define NOTIFICATION_NOTE_EDIT_TITLE @"NOTIFICATION_NOTE_EDIT_TITLE"



/**
 *  @author zm, 16-02-17 14:34:22
 *
 *  进入后台通知
 *
 */
#define NOTIFICATION_APPLICATION_DID_ENTER_BACKGROUND @"NOTIFICATION_APPLICATION_DID_ENTER_BACKGROUND"

/**
 *  @author zm, 16-02-17 14:34:22
 *
 *  中断通知
 *
 */
#define NOTIFICATION_APPLICATION_WILL_TERMINATE @"NOTIFICATION_APPLICATION_WILL_TERMINATE"

/**
 *  @author zm, 16-02-17 14:34:22
 *
 *  退出活动状态通知
 *
 */
#define NOTIFICATION_APPLICATION_WILL_RESIGNACTIVE @"NOTIFICATION_APPLICATION_WILL_RESIGNACTIVE"


/******************************** NOTIFICATION END *************************************/

/******************************** LOG *************************************/

#define zmStringLog(x) @#x, x
#define zmFormatLog(format) [@"\n%@:" stringByAppendingString:format]

#ifdef DEBUG
#define zmLog(format, x) NSLog(zmFormatLog(format), zmStringLog(x))
#define zmLog_Object(x) zmLog(@"%@", x)

#define zmLog_Size(size) NSLog(@"\n%@:{\n%@:  %f,\n%@: %f\n}", @#size, zmStringLog(size.width), zmStringLog(size.height))
#define zmLog_Frame(frame) NSLog(@"\n%@:{\n%@:    %f,\n%@:    %f,\n%@:  %f,\n%@: %f\n}", @#frame, zmStringLog(frame.origin.x), zmStringLog(frame.origin.y), zmStringLog(frame.size.width), zmStringLog(frame.size.height))
#else
#define zmLog(format, x) @""
#define zmLog_Object(x) zmLog(@"%@", x)

#define zmLog_Size(size) @""
#define zmLog_Frame(frame) @""
#endif
/******************************** LOG END *************************************/

#ifndef SCREEN_WIDTH
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#endif

#ifndef SCREEN_HEIGHT
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#endif

#ifndef IOS_VERSION
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#endif

#define ZCOLOR(r, g, b, a) [[UIColor alloc] initWithRed:(r)/255.f green:(g)/255.f blue:(b)/255.f alpha:a]

#define NAVIGATIONBAR_HEIGHT (CGRectGetHeight(self.navigationController.navigationBar.frame) + 20.f)

#define SEPARATOR_HEIGHT [ToolsManager getRealOnePixelHeight]

#define Orange_Color [UIColor colorWithRed:225.f / 255.f green:176.f / 255.f blue:99.f / 255.f alpha:1.f]
#define Gray_Color [UIColor colorWithRed:242.f / 255.f green:242.f / 255.f blue:242.f / 255.f alpha:1.f]
#define White_Color [UIColor colorWithWhite:1.f alpha:0.95f]
#define Separator_Color [UIColor colorWithWhite:0.f alpha:0.6f]

@interface ToolsManager : NSObject

+ (ToolsManager *)sharedTools;

#pragma mark - font or label
+ (void)fitWidthWithLabel:(UILabel *)label;
+ (void)fitHeightWithLabel:(UILabel *)label;
+ (void)fitSizeWithLabel:(UILabel *)label;

+ (CGFloat)getTextHeightWithFont:(UIFont *)font
                        andWidth:(CGFloat)width
                         andText:(NSString *)text;

#pragma mark - textView get last word location
- (CGPoint)getLastWordLocation:(NSMutableAttributedString *)text;

#pragma mark - textView or textField
+ (BOOL)textFieldJustNumberWithWordNumber:(NSInteger)maxWordNumber andTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

+ (BOOL)textFieldInputMaxWordNumber:(NSInteger)maxWordNumber andTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

//textView
+ (BOOL)textViewInputMaxWordNumber:(NSInteger)maxWordNumber andTextView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text andCutOff:(void(^)(void))CutOffBlock;

+ (void)textViewInputMaxWordNumber:(NSInteger)maxWordNumber andTextViewDidChange:(UITextView *)textView andCutOff:(void(^)(void))CutOffBlock;

#pragma mark - date
//from AdamWang

/**
 *  @author zm, 15-07-06 10:07:30
 *
 *  根据format返回
 *
 *  @param date   NSDate
 *  @param format 格式
 *
 *  @return string
 */
+ (NSString *)timeStringWithDate:(NSDate *)date andFormat:(NSString *)format;

/**
 *  根据format返回
 *
 *  @param stamp  时间戳
 *  @param format 格式
 *
 *  @return string
 */
+ (NSString *)timeStringWithStamp:(NSTimeInterval)stamp andFormat:(NSString *)format;

+ (NSString *)timeStringOfNowWithFormat:(NSString *)format;

/**
 *  比较两个NSDate是否在指定的时间间隔内
 *
 *  @param intervalTime      指定的时间间隔
 *  @param oneDate           一个date
 *  @param otherDate         另一个date
 *
 *  @return 在的话，返回yes；不在返回no
 */
+ (BOOL)compareInterval:(NSTimeInterval)intervalTime
            WithOneDate:(NSDate *)oneDate
         andAnotherDate:(NSDate *)otherDate;

/**
 *  获得指定日期0点的time
 *
 *  @param date 指定date
 *
 *  @return 时间戳
 */
+ (NSTimeInterval)zeroTimeOfDate:(NSDate *)date;

/**
 *  判断制定日期是否是今天
 *
 *  @param date 指定日期
 *
 *  @return yes or no
 */
+ (BOOL)isToday:(NSDate *)date;

#pragma mark - image
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size;

#pragma mark - alert
+(void)showAlertForNetworkUnstable;

+(void)showAlertWithText:(NSString *)text;

+(void)showAlertWithTitle:(NSString *)title andText:(NSString *)text;


+ (void)delayInSeconds:(CGFloat)seconds
          OnCompletion:(void(^)(void))completionBlock;

+ (BOOL)isSameColor1:(UIColor *)color1 color2:(UIColor *)color2;

#pragma mark - size
/**
 *  按比例缩小CGSize
 *
 *  @param size 要缩放的Size 当Size的一边为0时不做处理
 *  @param side 最大边的长度 当为0时不作处理, 当最大边的长度大于原大小时不作处理(不放大,只缩小)
 *
 *  @return 返回缩放后的CGSize
 */
+ (CGSize)zoomSize:(CGSize)size WithMaxSide:(CGFloat)side Scale:(CGFloat)scale;

+ (CGFloat)getRealOnePixelHeight;

+ (UIImage *)getResizeAndCircleImageWithImage:(UIImage *)originalImage
                                  andMaxWidth:(CGFloat)maxWidth
                                 andMaxHeight:(CGFloat)maxHeight
                                  andMinWidth:(CGFloat)minWidth
                                 andMinHeight:(CGFloat)minHeight
                              andCornerRadius:(CGFloat)cornerRadius
                               andTargetScale:(CGFloat)targetScale
                           andBackgroundColor:(UIColor *)backgroundColor;

@end
