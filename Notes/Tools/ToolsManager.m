//
//  ToolsManager.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "ToolsManager.h"

@interface ToolsManager ()

@property (nonatomic, strong) UITextView *textViewGetLastWordLocation;

@end

@implementation ToolsManager

+ (ToolsManager *)sharedTools
{
    static ToolsManager *tools = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tools = [[ToolsManager alloc] init];
    });
    
    return tools;
}

+ (void)fitWidthWithLabel:(UILabel *)label
{
    CGSize fitSize = [label.text boundingRectWithSize:CGSizeMake(MAXFLOAT, CGRectGetHeight(label.frame))
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:label.font}
                                              context:nil].size;
    
    CGRect fitFrame = label.frame;
    fitFrame.size.width = fitSize.width + 2.f;
    label.frame = fitFrame;
}

+ (void)fitHeightWithLabel:(UILabel *)label
{
    CGSize fitSize = [label.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(label.frame), MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:label.font}
                                              context:nil].size;
    
    CGRect fitFrame = label.frame;
    fitFrame.size.height = fitSize.height + 2.f;
    label.frame = fitFrame;
}

+ (void)fitSizeWithLabel:(UILabel *)label
{
    CGSize fitSize = [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}];
    
    CGRect fitFrame = label.frame;
    fitFrame.size.width = fitSize.width + 2.f;
    fitFrame.size.height = fitSize.height + 2.f;
    label.frame = fitFrame;
}

+ (CGFloat)getTextHeightWithFont:(UIFont *)font
                        andWidth:(CGFloat)width
                         andText:(NSString *)text
{
    CGSize fitSize = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:font}
                                        context:nil].size;
    
    return fitSize.height + 2.f;
}

#pragma mark - textField输入限制
//数字
+ (BOOL)textFieldJustNumberWithWordNumber:(NSInteger)maxWordNumber andTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= maxWordNumber && maxWordNumber > 0) {
        if ([string isEqualToString:@""]) {
            return YES;
        }
        if (range.length >= 1) {
            return [ToolsManager validateNumber:string];
        }
        return NO;
    }
    return [ToolsManager validateNumber:string];
    
}

//只能输入数字
+ (BOOL)validateNumber:(NSString*)number
{
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

+ (BOOL)textFieldInputMaxWordNumber:(NSInteger)maxWordNumber andTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= maxWordNumber) {
        if ([string isEqualToString:@""]) {
            return YES;
        }
        if (range.length >= 1) {
            return YES;
        }
        return NO;
    }
    return YES;
}

#pragma mark - textView

+ (BOOL)textViewInputMaxWordNumber:(NSInteger)maxWordNumber andTextView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text andCutOff:(void(^)(void))CutOffBlock
{
    if (textView.text.length >= maxWordNumber) {
        if ([text isEqualToString:@""]) {
            return YES;
        }
        if (range.length >= 1 && text.length <= range.length) {
            return YES;
        }
        
        if (textView.markedTextRange == nil) {
            textView.text = [textView.text substringToIndex:maxWordNumber];
            CutOffBlock();
            return NO;
        }
    }
    return YES;
}

+ (void)textViewInputMaxWordNumber:(NSInteger)maxWordNumber andTextViewDidChange:(UITextView *)textView andCutOff:(void(^)(void))CutOffBlock
{
    if (textView.markedTextRange == nil)
    {
        if (textView.text.length > maxWordNumber) {
            textView.text = [textView.text substringToIndex:maxWordNumber];
            CutOffBlock();
        }
    }
}

#pragma mark - date
+ (NSString *)timeStringWithDate:(NSDate *)date andFormat:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    return dateStr;
}

+ (NSString *)timeStringWithStamp:(NSTimeInterval)stamp andFormat:(NSString *)format
{   //yyyy/MM/ddHH/mm/ss
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:stamp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    return dateStr;
}

+ (NSString *)timeStringOfNowWithFormat:(NSString *)format
{
   return [[self class] timeStringWithDate:[NSDate date]
                                 andFormat:format];
}
/**
 *  比较两个NSDate是否在指定的时间间隔内
 *
 *  @param intervalTime      指定的时间间隔
 *  @param oneDate           一个date
 *  @param otherDate         另一个date
 *
 *  @return 在的话，返回yes；不在返回no
 */
+ (BOOL)compareInterval:(NSTimeInterval)intervalTime
            WithOneDate:(NSDate *)oneDate
         andAnotherDate:(NSDate *)otherDate
{
    BOOL flag = NO;
    
    NSTimeInterval oneTime = [oneDate timeIntervalSince1970];
    NSTimeInterval otherTime = [otherDate timeIntervalSince1970];
    
    NSTimeInterval differenceTime = otherTime - oneTime;
    
    if (fabs(differenceTime) < intervalTime) {
        flag = YES;
    }
    
    return flag;
}

/**
 *  获得指定日期0点的time
 *
 *  @param date 指定date
 *
 *  @return 时间戳
 */
+ (NSTimeInterval)zeroTimeOfDate:(NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comp = [gregorian components:NSUIntegerMax fromDate:date];
    comp.hour = 0;
    comp.minute = 0;
    comp.second = 0;
    
    NSTimeInterval zeroTime = [[gregorian dateFromComponents:comp] timeIntervalSince1970];
    
    return zeroTime;
}

/**
 *  判断制定日期是否是今天
 *
 *  @param date 指定日期
 *
 *  @return yes or no
 */
+ (BOOL)isToday:(NSDate *)date
{
    NSTimeInterval zeroTimeOfToday = [self zeroTimeOfDate:[NSDate date]];
    NSTimeInterval time = [date timeIntervalSince1970];
    
    if (time >= zeroTimeOfToday) {
        return YES;
    }
    
    return NO;
}

#pragma mark - image
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    
    CGContextFillRect(context, rect);
    
    
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
    return image;
    
}

#pragma mark - alert
+ (void)showAlertForNetworkUnstable
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"网络不稳定"
                                                                  message:@"连接服务器超时，请检查您的网络是否可用"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"确定"
                                                        otherButtonTitles:nil, nil];
    [alert show];
}

+(void)showAlertWithText:(NSString *)text
{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                  message:text
                                                                 delegate:nil
                                                        cancelButtonTitle:@"确定"
                                                        otherButtonTitles:nil, nil];
    [alter show];
}

+(void)showAlertWithTitle:(NSString *)title andText:(NSString *)text
{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:title
                                                                  message:text
                                                                 delegate:nil
                                                        cancelButtonTitle:@"确定"
                                                        otherButtonTitles:nil, nil];
    [alter show];
}

#pragma mark - textView get last word location
- (CGPoint)getLastWordLocation:(NSMutableAttributedString *)text
{
    if (self.textViewGetLastWordLocation == nil) {
        self.textViewGetLastWordLocation = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 20.f, 0)];
        self.textViewGetLastWordLocation.backgroundColor = [UIColor whiteColor];
        self.textViewGetLastWordLocation.font = [UIFont systemFontOfSize:15.f];
        self.textViewGetLastWordLocation.layoutManager.allowsNonContiguousLayout = NO;
    }
    
    self.textViewGetLastWordLocation.attributedText = text;
    
    return [self lastWordPoint];
}

- (CGPoint)lastWordPoint
{
    UITextRange *cursorLocationRange = [self.textViewGetLastWordLocation textRangeFromPosition:self.textViewGetLastWordLocation.beginningOfDocument toPosition:self.textViewGetLastWordLocation.endOfDocument];
    
    NSArray *arrFrame = [self.textViewGetLastWordLocation selectionRectsForRange:cursorLocationRange];
    
    if (arrFrame.count >= 2) {
        UITextSelectionRect *tFrame = arrFrame[0];
        UITextSelectionRect *tempFrame = arrFrame[1];
        if (CGRectGetWidth(tempFrame.rect) > 0) {
            
            return CGPointMake(CGRectGetMaxX(tempFrame.rect), CGRectGetMinY(tempFrame.rect));
            
        }
        else{
            
            return CGPointMake(CGRectGetMaxX(tFrame.rect), CGRectGetMinY(tFrame.rect));
            
        }
    }
    
    return CGPointZero;
}


+ (void)delayInSeconds:(CGFloat)seconds
          OnCompletion:(void(^)(void))completionBlock
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * 1000 * NSEC_PER_MSEC)), dispatch_get_main_queue(), ^{
        completionBlock();
    });
}

+ (BOOL)isSameColor1:(UIColor *)color1 color2:(UIColor *)color2
{
    CGFloat red1, green1, blue1, alpha1, red2, green2, blue2, alpha2;
    if ([color1 getRed:&red1 green:&green1 blue:&blue1 alpha:&alpha1]) {
        if ([color2 getRed:&red2 green:&green2 blue:&blue2 alpha:&alpha2]) {
            
            
            if (red1 == red2 &&
                green1 == green2 &&
                blue1 == blue2 &&
                alpha1 == alpha2) {
                return YES;
            }
        }
    }
    
    return NO;
}

#pragma mark - size
/**
 *  按比例缩小CGSize
 *
 *  @param size 要缩放的Size 当Size的一边为0时不做处理
 *  @param side 最大边的长度 当为0时不作处理, 当最大边的长度大于原大小时不作处理(不放大,只缩小)
 *
 *  @return 返回缩放后的CGSize
 */
+ (CGSize)zoomSize:(CGSize)size WithMaxSide:(CGFloat)side Scale:(CGFloat)scale
{
    if (size.width == 0 || size.height == 0 || side == 0) {
        return size;
    }
    float ratio = side/size.width;
    if (ratio > side/size.height) {
        ratio = side/size.height;
    }
    if (ratio >= 1) {
        size.width  /= scale;
        size.height /= scale;
        return size;
    }
    size.width  = size.width * ratio;
    size.height = size.height * ratio;
    
    size.width  /= scale;
    size.height /= scale;
    
    return size;
}

+ (CGFloat)getRealOnePixelHeight
{
    return 1.f / ([UIScreen mainScreen].scale);
}

+ (UIImage *)getResizeAndCircleImageWithImage:(UIImage *)originalImage
                                  andMaxWidth:(CGFloat)maxWidth
                                 andMaxHeight:(CGFloat)maxHeight
                                  andMinWidth:(CGFloat)minWidth
                                 andMinHeight:(CGFloat)minHeight
                              andCornerRadius:(CGFloat)cornerRadius
                               andTargetScale:(CGFloat)targetScale
                           andBackgroundColor:(UIColor *)backgroundColor
{
    if (!originalImage)
    {
        return nil;
    }
    
    CGFloat originalImageWidth = originalImage.size.width * originalImage.scale / targetScale;
    CGFloat originalImageHeight = originalImage.size.height * originalImage.scale / targetScale;
    
    CGFloat scaleW;
    CGFloat scaleH;
    
    CGFloat targetScaleW = 1.f;
    CGFloat targetScaleH = 1.f;
    
    if (originalImageWidth > maxWidth || originalImageHeight > maxHeight) //如果大于最大尺寸
    {
        scaleW = maxWidth / originalImageWidth;
        scaleH = maxHeight / originalImageHeight;
        
        targetScaleW = MIN(scaleW, scaleH);
        targetScaleH = targetScaleW;
    }
    
    if ((originalImageWidth * targetScaleW) < minWidth || (originalImageHeight * targetScaleH) < minHeight) //如果小于最小尺寸
    {
        scaleW = minWidth / (originalImageWidth * targetScaleW);
        scaleH = minHeight / (originalImageHeight * targetScaleH);
        
        targetScaleW = targetScaleW * MAX(scaleW, scaleH);
        targetScaleH = targetScaleW;
    }
    
    CGFloat targetWidth = MAX(MIN(originalImageWidth * targetScaleW, maxWidth), minWidth);
    CGFloat targetHeight = MAX(MIN(originalImageHeight * targetScaleH, maxHeight), minHeight);
    
    CGFloat resizeImageWidth = originalImageWidth * targetScaleW;
    CGFloat resizeImageHeight = originalImageHeight * targetScaleH;
    
    CGFloat targetCornerRadius = MIN(MIN(targetWidth / 2.f, targetHeight / 2.f), cornerRadius);
    
    //中心对齐
    CGRect resizeImageRect = CGRectMake((targetWidth - resizeImageWidth) / 2.f, (targetHeight - resizeImageHeight) / 2.f, resizeImageWidth, resizeImageHeight);
    
    CGRect targetRect = CGRectMake(0.f, 0.f, targetWidth, targetHeight);
    
    UIGraphicsBeginImageContextWithOptions(targetRect.size, false, targetScale);
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(contextRef, [UIColor clearColor].CGColor);
    
    UIRectFill(targetRect);
    
    CGContextAddRoundRect(contextRef, 0.f, 0.f, targetRect.size.width, targetRect.size.height, targetCornerRadius);
    
    CGContextClip(contextRef);
    
    //绘制白色背景
    CGContextSetFillColorWithColor(contextRef, backgroundColor.CGColor);
    CGContextFillRect(contextRef, targetRect);
    
    [originalImage drawInRect:resizeImageRect];
    
    UIImage* targetImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return targetImage;
}

void CGContextAddRoundRect(CGContextRef c, CGFloat x1 , CGFloat y1
                           , CGFloat width , CGFloat height , CGFloat radius)
{
    CGContextMoveToPoint(c, x1 + radius , y1);
    CGContextAddLineToPoint(c, x1 + width - radius, y1);
    CGContextAddArcToPoint(c, x1 + width, y1, x1 + width, y1 + radius, radius);
    CGContextAddLineToPoint(c, x1 + width, y1 + height - radius);
    CGContextAddArcToPoint(c, x1 + width, y1 + height, x1 + width - radius, y1 + height, radius);
    CGContextAddLineToPoint(c, x1 + radius, y1 + height);
    CGContextAddArcToPoint(c, x1, y1 + height, x1, y1 + height - radius, radius);
    CGContextAddLineToPoint(c, x1, y1 + radius);
    CGContextAddArcToPoint(c, x1, y1, x1 + radius, y1, radius);
}
@end
