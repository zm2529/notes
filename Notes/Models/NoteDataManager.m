//
//  NoteDataManager.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteDataManager.h"

#import "NoteData.h"
#import "StorageNotesManager.h"

static NSString *const STORAGE_KEY_FOLDER_LIST = @"STORAGE_KEY_FOLDER_LIST";

@interface NoteDataManager ()

@property (nonatomic, strong, readwrite) NoteData *lastEidtNote;
@property (nonatomic, strong, readwrite) NoteFolderData *lastEditFolderData;

@property (nonatomic, strong) StorageNotesManager *storageManager;

@property (nonatomic, strong) NoteFolderListData *folderListData;

@end

@implementation NoteDataManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.storageManager = [[StorageNotesManager alloc] init];
        
        [self loadFolderList];
    }
    
    return self;
}

+ (NoteDataManager *)sharedManager
{
    static NoteDataManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[NoteDataManager alloc] init];
    });
    
    return manager;
}

- (void)loadFolderList
{
    self.folderListData = [NSKeyedUnarchiver unarchiveObjectWithData:[self.storageManager getSomethingWithKey:STORAGE_KEY_FOLDER_LIST]];
    if (self.folderListData == nil) {
        self.folderListData = [[NoteFolderListData alloc] init];
    }
}

#pragma mark - storage
- (void)saveNote:(NoteData *)noteData
{
//    [self.storageManager saveSomething:[NSKeyedArchiver archivedDataWithRootObject:noteData] andKey:noteData.noteID];
    [self saveAll];
}

- (void)saveNoteWithID:(NSString *)noteID
{
    NoteData *note = [self getNoteDataWithNoteID:noteID];
    
    [self saveNote:note];
}

- (void)saveEidtingNote
{
    [self saveNote:self.lastEidtNote];
}

- (void)saveAll
{
    [self.storageManager saveSomething:[NSKeyedArchiver archivedDataWithRootObject:self.folderListData] andKey:STORAGE_KEY_FOLDER_LIST];
}

#pragma mark - folder list
- (NSArray *)getFolderList
{
    if (self.folderListData == nil) {
        [self loadFolderList];
    }
    return [self.folderListData.arrFolders copy];
}

#pragma mark - folder
- (NoteFolderData *)addNewFolder
{
    return [self.folderListData createFolder];
}

- (void)addNote:(NoteData *)noteData toFolder:(NSString *)folderID
{
    NoteFolderData *folder = [self getNoteFolderWithFolderID:folderID];
    [folder addANote:noteData];
}

- (void)renameFolderWithID:(NSString *)folderID andNewName:(NSString *)newName
{
    NoteFolderData *folder = [self getNoteFolderWithFolderID:folderID];
    [folder rename:newName];
}

- (void)removeFolderWithID:(NSString *)folderID
{
    NoteFolderData *folder = [self getNoteFolderWithFolderID:folderID];
    [self.folderListData removeFolder:folder];
}

- (NSArray *)getNoteListWithFolerID:(NSString *)folderID
{
    NoteFolderData *folder = [self getNoteFolderWithFolderID:folderID];
    self.lastEditFolderData = folder;
    if (folder.arrNotes == nil) {
        return nil;
    }
    return self.lastEditFolderData.arrNotes;
}

- (NoteFolderData *)getNoteFolderWithFolderID:(NSString *)folderID
{
    for (NoteFolderData *folder in self.folderListData.arrFolders) {
        if ([folder.folderID isEqualToString:folderID]) {
            return folder;
        }
    }
    
    return nil;
}


#pragma mark - note
- (NoteData *)addNewNote
{
    return [self.lastEditFolderData createNote];
}

- (void)changeNoteTitle:(NSString *)title andID:(NSString *)noteID
{
    NoteData *note = [self getNoteDataWithNoteID:noteID];
    [note updateTitle:title];
}

- (void)removeNoteWithID:(NSString *)noteID
{
    NoteData *note = [self getNoteDataWithNoteID:noteID];
    [self.lastEditFolderData removeNote:note];
}

- (NoteData *)getNoteDataWithNoteID:(NSString *)noteID
{
    for (NoteData *note in self.lastEditFolderData.arrNotes) {
        if ([note.noteID isEqualToString:noteID]) {
            
            self.lastEidtNote = note;
            return note;
        }
    }
    
    for (NoteFolderData *folder in self.folderListData.arrFolders) {
        for (NoteData *note in folder.arrNotes) {
            if ([note.noteID isEqualToString:noteID]) {
                self.lastEditFolderData = folder;
                self.lastEidtNote = note;
                return note;
            }
        }
    }
    
    return nil;
}

@end
