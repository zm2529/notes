//
//  StorageManager.h
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StorageNotesManager : NSObject

- (void)saveSomething:(id)data andKey:(NSString *)key;

- (id)getSomethingWithKey:(NSString *)key;

@end
