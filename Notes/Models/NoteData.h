//
//  NoteData.h
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteEditHistory : NSObject<NSCoding>

@property (nonatomic, strong) NSString *noteInfoID;

@property (nonatomic, copy) NSAttributedString *text;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *changeTitle;
@property (nonatomic, strong) NSString *editTime;

@property (nonatomic, strong) NSString *recoveryFromDate;

@end

@interface NoteInfoData : NSObject<NSCoding>

@property (nonatomic, strong, readonly) NSString *createDate;
@property (nonatomic, strong) NSString *lastEditDate;
@property (nonatomic, strong) NSString *wordNumbers;
@property (nonatomic, strong) NSString *superFolderID;
@property (nonatomic, strong, readonly) NSString *noteInfoID;

@property (nonatomic, strong) NSString *noteTitle;

@property (nonatomic, strong) UIFont *font;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, strong) NSMutableArray *arrEditHistory;

@end



@interface NoteData : NSObject<NSCoding>

@property (nonatomic, copy) NSAttributedString *text;

@property (nonatomic, strong, readonly) NSString *noteID;

@property (nonatomic, strong, readonly) NoteInfoData *noteInfo;

- (void)reloadWithNote:(NSAttributedString *)text;

- (void)recoveryWithNote:(NSAttributedString *)text andFromDate:(NSString *)date;

- (void)updateTitle:(NSString *)title;


@end


@interface NoteFolderData : NSObject<NSCoding>

@property (nonatomic, strong, readonly) NSMutableArray *arrNotes;

@property (nonatomic, strong, readonly) NSString *createFolderDate;
@property (nonatomic, strong, readonly) NSString *folderID;
@property (nonatomic, strong) NSString *folderTitle;

- (NoteData *)createNote;

- (void)removeNote:(NoteData *)noteData;

- (void)rename:(NSString *)newName;

- (void)addANote:(NoteData *)noteData;

@end


@interface NoteFolderListData : NSObject<NSCoding>

@property (nonatomic, strong, readonly) NSMutableArray *arrFolders;

- (NoteFolderData *)createFolder;
- (void)removeFolder:(NoteFolderData *)folderData;

@end
