//
//  NoteDataManager.h
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NoteData;
@class NoteFolderData;

@interface NoteDataManager : NSObject

@property (nonatomic, strong, readonly) NoteData *lastEidtNote;

+ (NoteDataManager *)sharedManager;

#pragma mark - storage
- (void)saveNote:(NoteData *)noteData;
- (void)saveNoteWithID:(NSString *)noteID;

- (void)saveEidtingNote;

- (void)saveAll;


#pragma mark - folder list
- (NSArray *)getFolderList;

#pragma mark - folder
- (NoteFolderData *)addNewFolder;
- (void)addNote:(NoteData *)noteData toFolder:(NSString *)folderID;
- (void)renameFolderWithID:(NSString *)folderID andNewName:(NSString *)newName;
- (void)removeFolderWithID:(NSString *)folderID;

- (NSArray *)getNoteListWithFolerID:(NSString *)folderID;
- (NoteFolderData *)getNoteFolderWithFolderID:(NSString *)folderID;

#pragma mark - note
- (NoteData *)addNewNote;
- (void)changeNoteTitle:(NSString *)title andID:(NSString *)noteID;
- (void)removeNoteWithID:(NSString *)noteID;

- (NoteData *)getNoteDataWithNoteID:(NSString *)noteID;

@end
