//
//  NoteData.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteData.h"
#import "ToolsManager.h"

static NSString *const DATE_FORMAT = @"yyyy年MM月dd日  HH:mm:ss";

#pragma mark - 

@implementation NoteEditHistory

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.text = [aDecoder decodeObjectForKey:@"text"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.changeTitle = [aDecoder decodeObjectForKey:@"changeTitle"];
        self.editTime = [aDecoder decodeObjectForKey:@"editTime"];
        self.recoveryFromDate = [aDecoder decodeObjectForKey:@"recoveryFromDate"];
        self.noteInfoID = [aDecoder decodeObjectForKey:@"noteInfoID"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.text forKey:@"text"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.changeTitle forKey:@"changeTitle"];
    [aCoder encodeObject:self.editTime forKey:@"editTime"];
    [aCoder encodeObject:self.recoveryFromDate forKey:@"recoveryFromDate"];
    [aCoder encodeObject:self.noteInfoID forKey:@"noteInfoID"];
}


@end

#pragma mark - NoteInfoData

@interface NoteInfoData ()

@property (nonatomic, strong, readwrite) NSString *createDate;
@property (nonatomic, strong, readwrite) NSString *noteInfoID;

@end

@implementation NoteInfoData

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.noteInfoID = [NSString stringWithFormat:@"noteinfo-%f", [[NSDate date] timeIntervalSince1970]];
        
        self.createDate = [ToolsManager timeStringOfNowWithFormat:DATE_FORMAT];
        
        self.lastEditDate = [ToolsManager timeStringOfNowWithFormat:DATE_FORMAT];

        self.wordNumbers = @"0";
        
        self.noteTitle = @"";
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.createDate = [aDecoder decodeObjectForKey:@"createDate"];
        self.lastEditDate = [aDecoder decodeObjectForKey:@"lastEditDate"];
        self.superFolderID = [aDecoder decodeObjectForKey:@"superFolderID"];
        self.noteInfoID = [aDecoder decodeObjectForKey:@"noteInfoID"];
        self.noteTitle = [aDecoder decodeObjectForKey:@"noteTitle"];
        self.font = [aDecoder decodeObjectForKey:@"font"];
        self.color = [aDecoder decodeObjectForKey:@"color"];
        self.wordNumbers = [aDecoder decodeObjectForKey:@"wordNumbers"];
        self.arrEditHistory = [aDecoder decodeObjectForKey:@"arrEditHistory"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    /*
     @property (nonatomic, strong, readonly) NSString *createDate;
     @property (nonatomic, strong) NSString *lastEditDate;
     @property (nonatomic, strong) NSString *wordNumbers;
     @property (nonatomic, strong) NSString *superFolderID;
     @property (nonatomic, strong, readonly) NSString *noteInfoID;
     
     @property (nonatomic, strong) UIFont *font;
     
     @property (nonatomic, strong) UIColor *color;
     */
    [aCoder encodeObject:self.createDate forKey:@"createDate"];
    [aCoder encodeObject:self.lastEditDate forKey:@"lastEditDate"];
    [aCoder encodeObject:self.superFolderID forKey:@"superFolderID"];
    [aCoder encodeObject:self.noteInfoID forKey:@"noteInfoID"];
    [aCoder encodeObject:self.noteTitle forKey:@"noteTitle"];
    [aCoder encodeObject:self.font forKey:@"font"];
    [aCoder encodeObject:self.color forKey:@"color"];
    [aCoder encodeObject:self.wordNumbers forKey:@"wordNumbers"];
    [aCoder encodeObject:self.arrEditHistory forKey:@"arrEditHistory"];
}

- (void)addAEditInfoWithText:(NSAttributedString *)text
              andChangeTitle:(NSString *)changeTitle
          andRecoverFromDate:(NSString *)recoverFromDate
{
    self.lastEditDate = [ToolsManager timeStringOfNowWithFormat:DATE_FORMAT];
    
    self.wordNumbers = [NSString stringWithFormat:@"%d", (int)text.length];
    
    if (self.arrEditHistory == nil) {
        self.arrEditHistory = [[NSMutableArray alloc] init];
    }
    
    NoteEditHistory *editHistory = [[NoteEditHistory alloc] init];
    if (editHistory != nil) {
        editHistory.title = self.noteTitle;
        editHistory.changeTitle = changeTitle;
        editHistory.text = text;
        editHistory.editTime = self.lastEditDate;
        editHistory.recoveryFromDate = recoverFromDate;
        editHistory.noteInfoID = self.noteInfoID;
        [self.arrEditHistory insertObject:editHistory atIndex:0];
    }
}

@end





#pragma mark - NoteData

@interface NoteData ()

@property (nonatomic, strong, readwrite) NSString *noteID;

@property (nonatomic, strong, readwrite) NoteInfoData *noteInfo;

@end

@implementation NoteData

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.noteID = [NSString stringWithFormat:@"note-%f", [[NSDate date] timeIntervalSince1970]];

        self.text = [[NSAttributedString alloc] initWithString:@""];
        
        self.noteInfo = [[NoteInfoData alloc] init];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.text = [aDecoder decodeObjectForKey:@"text"];
        self.noteID = [aDecoder decodeObjectForKey:@"noteID"];
        self.noteInfo = [aDecoder decodeObjectForKey:@"noteInfo"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    /*
     @property (nonatomic, strong) NSString *noteTitle;
     
     @property (nonatomic, strong) NSString *note;
     
     @property (nonatomic, strong, readonly) NSString *noteID;
     
     @property (nonatomic, strong, readonly) NoteInfoData *noteInfo;
     */
    [aCoder encodeObject:self.text forKey:@"text"];
    [aCoder encodeObject:self.noteID forKey:@"noteID"];
    [aCoder encodeObject:self.noteInfo forKey:@"noteInfo"];
}


- (void)reloadWithNote:(NSAttributedString *)text
{
//    NSRange range = NSMakeRange(0, self.text.length);
//    NSRange newRange = NSMakeRange(0, text.length);
//    
//    NSDictionary *dicAttributed = [self.text attributesAtIndex:0 effectiveRange:&range];
//    NSDictionary *dicNewAttributed = [text attributesAtIndex:0 effectiveRange:&newRange];
//    
//    if ((![self.text.string isEqualToString:text.string]) || (![dicAttributed isEqualToDictionary:dicNewAttributed])) {
    
    
    if (![self.text isEqualToAttributedString:text]) {
        [self.noteInfo addAEditInfoWithText:text
                             andChangeTitle:nil
                         andRecoverFromDate:nil];
    }
    
    self.text = text;
}

- (void)recoveryWithNote:(NSAttributedString *)text andFromDate:(NSString *)date
{
    [self.noteInfo addAEditInfoWithText:text
                         andChangeTitle:nil
                     andRecoverFromDate:date];
    
    self.text = text;
}

- (void)updateTitle:(NSString *)title
{
    if ( (![self.noteInfo.noteTitle isEqualToString:title]) && title != nil) {
        self.noteInfo.noteTitle = title;
        
        [self.noteInfo addAEditInfoWithText:self.text
                             andChangeTitle:title
                         andRecoverFromDate:nil];
    }
}

@end


#pragma mark - NoteFolderData

@interface NoteFolderData ()

@property (nonatomic, strong, readwrite) NSMutableArray *arrNotes;

@property (nonatomic, strong, readwrite) NSString *createFolderDate;
@property (nonatomic, strong, readwrite) NSString *folderID;

@end

@implementation NoteFolderData

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.arrNotes = nil;
        
        self.folderTitle = @"";
        
        self.createFolderDate = [ToolsManager timeStringOfNowWithFormat:DATE_FORMAT];
        
        self.folderID = [NSString stringWithFormat:@"folder-%f", [[NSDate date] timeIntervalSince1970]];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.arrNotes = [aDecoder decodeObjectForKey:@"arrNotes"];
        self.createFolderDate = [aDecoder decodeObjectForKey:@"createFolderDate"];
        self.folderID = [aDecoder decodeObjectForKey:@"folderID"];
        self.folderTitle = [aDecoder decodeObjectForKey:@"folderTitle"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    /*
     @property (nonatomic, strong, readonly) NSMutableArray *arrNotes;
     
     @property (nonatomic, strong, readonly) NSString *createFolderDate;
     @property (nonatomic, strong, readonly) NSString *folderID;
     @property (nonatomic, strong) NSString *folderTitle;
     */
    [aCoder encodeObject:self.arrNotes forKey:@"arrNotes"];
    [aCoder encodeObject:self.createFolderDate forKey:@"createFolderDate"];
    [aCoder encodeObject:self.folderID forKey:@"folderID"];
    [aCoder encodeObject:self.folderTitle forKey:@"folderTitle"];
}

- (NoteData *)createNote
{
    NoteData *noteData = [[NoteData alloc] init];
    
    if (self.arrNotes == nil) {
        self.arrNotes = [[NSMutableArray alloc] init];
    }
    
    if (noteData != nil) {
        noteData.noteInfo.superFolderID = self.folderID;
        [self.arrNotes addObject:noteData];
    }
    
    return noteData;
}

- (void)removeNote:(NoteData *)noteData
{
    if (noteData != nil) {
        [self.arrNotes removeObject:noteData];
    }
}

- (void)addANote:(NoteData *)noteData
{
    if (noteData != nil) {
        if (self.arrNotes == nil) {
            self.arrNotes = [[NSMutableArray alloc] init];
        }
        [self.arrNotes addObject:noteData];
    }
}

- (void)rename:(NSString *)newName
{
    self.folderTitle = newName;
}

@end


#pragma mark - NoteListData

@interface NoteFolderListData ()

@property (nonatomic, strong, readwrite) NSMutableArray *arrFolders;

@end

@implementation NoteFolderListData

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.arrFolders = nil;
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.arrFolders = [aDecoder decodeObjectForKey:@"arrFolders"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    /*
     @property (nonatomic, strong, readonly) NSMutableArray *arrFolders;
     */
    [aCoder encodeObject:self.arrFolders forKey:@"arrFolders"];
}

- (NoteFolderData *)createFolder
{
    NoteFolderData *folderData = [[NoteFolderData alloc] init];
    
    if (self.arrFolders == nil) {
        self.arrFolders = [[NSMutableArray alloc] init];
    }
    
    if (folderData != nil) {
        [self.arrFolders addObject:folderData];
    }
    
    return folderData;
}

- (void)removeFolder:(NoteFolderData *)folderData
{
    if (folderData != nil) {
        [self.arrFolders removeObject:folderData];
    }
}

@end
