//
//  StorageManager.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "StorageNotesManager.h"

#import "NoteData.h"

@implementation StorageNotesManager

- (void)saveSomething:(id)data andKey:(NSString *)key
{
    if (data != nil) {
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setObject:data forKey:key];
        [userDefaults synchronize];
    }
}

- (id)getSomethingWithKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id data = [userDefaults objectForKey:key];

    return data;
}

@end
