//
//  NoteSettingViewController.m
//  Notes
//
//  Created by zm on 2/15/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteSettingViewController.h"

#import "ToolsManager.h"

@interface NoteSettingViewController ()

@property (nonatomic, strong) UITableView *settingTableView;

@end

@implementation NoteSettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createUI];
}

- (void)createUI
{
    self.view.backgroundColor = Gray_Color;
    
    
}

@end
