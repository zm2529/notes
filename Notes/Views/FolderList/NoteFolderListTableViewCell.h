//
//  NoteFolderListTableViewCell.h
//  Notes
//
//  Created by zm on 2/15/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NotesCommentTableViewCell.h"

@class NoteFolderData;

@interface NoteFolderListTableViewCell : NotesCommentTableViewCell

@property (nonatomic, strong) NoteFolderData *folderData;

- (void)reloadData;

+ (CGFloat)cellHeight;

@end
