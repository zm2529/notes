//
//  NoteListViewController.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteFolderListViewController.h"

#import "AppDelegate.h"

#import "ToolsManager.h"

#import "NoteData.h"
#import "NoteDataManager.h"
#import "NoteListViewController.h"
#import "NoteFolderListTableViewCell.h"

@interface NoteFolderListViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *noteFolderListTableView;

@property (nonatomic, copy) NSArray *arrFolders;

@end

@implementation NoteFolderListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createUI];
}

- (void)createUI
{
    
//    self.view.backgroundColor = Gray_Color;
    
    UIBarButtonItem *btnAddNewFolder = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                        target:self
                                        action:@selector(addNewFolder)];
    [btnAddNewFolder setTintColor:Orange_Color];
    self.navigationItem.rightBarButtonItem = btnAddNewFolder;
    
    [self setTitle:@"文件夹"];
    

    
    self.noteFolderListTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                           0,
                                                                           SCREEN_WIDTH,
                                                                           SCREEN_HEIGHT)];
    self.noteFolderListTableView.delegate = self;
    self.noteFolderListTableView.dataSource = self;
    self.noteFolderListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.noteFolderListTableView];

}

- (void)viewWillAppear:(BOOL)animated
{
    [self.noteFolderListTableView reloadData];
}

- (void)addNewFolder
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"新建文件夹"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"";
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NoteFolderData *folder = [[NoteDataManager sharedManager] addNewFolder];
        UITextField *txtFolderTitle = alertController.textFields.firstObject;
        folder.folderTitle = txtFolderTitle.text;
        
        [self.noteFolderListTableView reloadData];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [alertController.view setNeedsLayout];
    [self presentViewController:alertController animated:YES completion:nil];

}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.arrFolders = [[NoteDataManager sharedManager] getFolderList];
    return self.arrFolders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *FolderListCell = @"FolderListCell";
    NoteFolderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FolderListCell];
    if (cell == nil) {
        cell = [[NoteFolderListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:FolderListCell];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.arrFolders.count) {
        NoteFolderData *folder = self.arrFolders[indexPath.row];
//        cell.textLabel.text = @"folder";
        if ([cell isKindOfClass:[NoteFolderListTableViewCell class]]) {
            NoteFolderListTableViewCell *folderCell = (NoteFolderListTableViewCell *)cell;
            [folderCell setCellIndex:indexPath.row andAllCellCount:self.arrFolders.count];
            folderCell.folderData = folder;
            [folderCell reloadData];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.arrFolders.count) {
        NoteFolderData *folder = self.arrFolders[indexPath.row];
        if (ApplicationDelegate.noteListVC == nil) {
            ApplicationDelegate.noteListVC = [[NoteListViewController alloc] init];
        }
        [self.navigationController pushViewController:ApplicationDelegate.noteListVC animated:YES];
        ApplicationDelegate.noteListVC.folderData = folder;
        ApplicationDelegate.noteListVC.title = folder.folderTitle;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [NoteFolderListTableViewCell cellHeight];
}
@end
