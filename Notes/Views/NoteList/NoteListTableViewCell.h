//
//  NoteListTableViewCell.h
//  Notes
//
//  Created by zm on 2/15/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NotesCommentTableViewCell.h"

@class NoteData;

@interface NoteListTableViewCell : NotesCommentTableViewCell

@property (nonatomic, strong) NoteData *noteData;

- (void)reloadData;

+ (CGFloat)cellHeight;


@end
