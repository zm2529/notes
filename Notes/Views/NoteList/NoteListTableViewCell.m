//
//  NoteListTableViewCell.m
//  Notes
//
//  Created by zm on 2/15/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteListTableViewCell.h"

#import "ScreenAdaptationHelper.h"

#import "NoteData.h"
#import "ToolsManager.h"

@interface NoteListTableViewCell ()

@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UILabel *lblSubTitle;
@property (nonatomic, strong) UILabel *lblNoteNumbers;
@property (nonatomic, strong) UILabel *lblCreateDate;
@property (nonatomic, strong) UILabel *lblLastEditDate;

@end

@implementation NoteListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    
    return self;
}

- (void)createUI
{
    //title
    {
        self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 5.f, 0, 20.f)];
        self.lblTitle.backgroundColor = [UIColor clearColor];
        self.lblTitle.textAlignment = NSTextAlignmentLeft;
        self.lblTitle.font = [UIFont systemFontOfSize:20.f];
        [self.contentView addSubview:self.lblTitle];
    }
    
    //subtitle
    {
        self.lblSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 0, 0, 20.f)];
        self.lblSubTitle.backgroundColor = [UIColor clearColor];
        self.lblSubTitle.textAlignment = NSTextAlignmentLeft;
        self.lblSubTitle.font = [UIFont systemFontOfSize:13.f];
        [self.contentView addSubview:self.lblSubTitle];
    }
    
    //numbers
    {
        self.lblNoteNumbers = [[UILabel alloc] init];
        self.lblNoteNumbers.backgroundColor = [UIColor clearColor];
        self.lblNoteNumbers.textAlignment = NSTextAlignmentRight;
        self.lblNoteNumbers.font = [UIFont systemFontOfSize:12.f];
        [self.contentView addSubview:self.lblNoteNumbers];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    [ScreenAdaptationHelper locationLayoutWithAlignment:EnumLocationLayoutAlignmentRight
                                                   view:self.lblNoteNumbers
                                                 offset:CGPointMake(-15.f, 0)
                                          superViewSize:self.contentView.frame.size];
}

- (void)reloadData
{
    if (self.noteData != nil) {
        self.lblTitle.text = self.noteData.noteInfo.noteTitle;
        [ToolsManager fitSizeWithLabel:self.lblTitle];
        
        self.lblSubTitle.text = self.noteData.noteInfo.createDate;
        [ToolsManager fitSizeWithLabel:self.lblSubTitle];
        
        [ScreenAdaptationHelper verticalAutoSpaceWithViews:@[
                                                             self.lblTitle,
                                                             self.lblSubTitle,
                                                             ]
                               equalSpaceLayoutContentMode:EnumEqualSpaceLayoutContentModeCenter
                                             superViewSize:self.contentView.frame.size
                                                  topSpace:0
                                               bottomSpace:0];
    }
}

+ (CGFloat)cellHeight
{
    return 80.f;
}


@end
