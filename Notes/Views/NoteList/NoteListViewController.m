//
//  NoteListViewController.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteListViewController.h"

#import "AppDelegate.h"

#import "ToolsManager.h"

#import "NoteData.h"
#import "NoteDataManager.h"
#import "NoteViewController.h"
#import "NoteListTableViewCell.h"

@interface NoteListViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *noteListTableView;

@property (nonatomic, copy) NSArray *arrNotes;

@end

@implementation NoteListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createUI];
}

- (void)createUI
{
    
//    self.view.backgroundColor = Gray_Color;
    
    UIBarButtonItem *btnAddNewFolder = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                        target:self
                                        action:@selector(addNewFolder)];
    self.navigationItem.rightBarButtonItem = btnAddNewFolder;
    [btnAddNewFolder setTintColor:Orange_Color];
    
    self.noteListTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                           0,
                                                                           SCREEN_WIDTH,
                                                                           SCREEN_HEIGHT)];
    self.noteListTableView.delegate = self;
    self.noteListTableView.dataSource = self;
    self.noteListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.noteListTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.noteListTableView reloadData];
}

- (void)addNewFolder
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"新建备忘录"
                                                                             message:@""
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"";
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NoteData *note = [[NoteDataManager sharedManager] addNewNote];;
        UITextField *txtNoteTitle = alertController.textFields.firstObject;
        note.noteInfo.noteTitle = txtNoteTitle.text;
        
        [self.noteListTableView reloadData];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [alertController.view setNeedsLayout];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.arrNotes = [[NoteDataManager sharedManager] getNoteListWithFolerID:self.folderData.folderID];
    return self.arrNotes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *NoteListCell = @"NoteListCell";
    NoteListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NoteListCell];
    if (cell == nil) {
        cell = [[NoteListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:NoteListCell];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.arrNotes.count) {
        NoteData *note = self.arrNotes[indexPath.row];
        //        cell.textLabel.text = @"folder";
//        cell.textLabel.text = note.noteInfo.createDate;
        if ([cell isKindOfClass:[NoteListTableViewCell class]]) {
            NoteListTableViewCell *noteCell = (NoteListTableViewCell *)cell;
            [noteCell setCellIndex:indexPath.row andAllCellCount:self.arrNotes.count];
            noteCell.noteData = note;
            [noteCell reloadData];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.arrNotes.count) {
        NoteData *note = self.arrNotes[indexPath.row];
        
        if (ApplicationDelegate.noteVC == nil) {
            ApplicationDelegate.noteVC = [[NoteViewController alloc] init];
        }
        [self.navigationItem.backBarButtonItem setTintColor:Orange_Color];
        ApplicationDelegate.noteVC.note = note;
        [self.navigationController pushViewController:ApplicationDelegate.noteVC animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [NoteListTableViewCell cellHeight];
}
@end
