//
//  ImagePicker.h
//  Notes
//
//  Created by zm on 2/17/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePicker : NSObject

//判断此设备是否支持摄像头
+ (BOOL)isCameraDeviceAvailable;

- (void)displayOperateActionSheetWithAllowEdit:(BOOL)allowEdit
                                andFinishBlock:(void(^)(UIImage *pickImage, NSString *extension))finish;

- (void)pickImageWithType:(UIImagePickerControllerSourceType)sourceType
             andAllowEdit:(BOOL)allowEdit
           andFinishBlock:(void(^)(UIImage *pickImage, NSString *extension))finish;

- (void)dismiss;

@end
