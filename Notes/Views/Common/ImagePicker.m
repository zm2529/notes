//
//  ImagePicker.m
//  Notes
//
//  Created by zm on 2/17/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "ImagePicker.h"

#import <AVFoundation/AVFoundation.h>

#import "ToolsManager.h"
#import "AppDelegate.h"

@interface ImagePicker () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) UIAlertController *actionSheetPictureSource;

@property (nonatomic, strong) UIImagePickerController *imagePicker;

@property (nonatomic, assign, getter=isPickerAllowEdit) BOOL pickerAllowEdit;
@property (nonatomic, assign, getter=isClose) BOOL close;

@property (nonatomic, copy) void(^finishBlock)(UIImage *pickImage, NSString *extension);

@end

@implementation ImagePicker

-(instancetype)init
{
    self = [super init];
    
    if(self){
        self.close = YES;
    }
    return self;
}

- (void)displayOperateActionSheetWithAllowEdit:(BOOL)allowEdit
                                andFinishBlock:(void(^)(UIImage *pickImage, NSString *extension))finish
{
    self.pickerAllowEdit = allowEdit;
    self.finishBlock = finish;
    
    if (self.actionSheetPictureSource == nil) {
        self.actionSheetPictureSource = [UIAlertController alertControllerWithTitle:@"还tm要图片？！！"
                                                                            message:@"你要疯啊！！"
                                                                     preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"我错了！"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                             }];
        
        
        UIAlertAction *photosAlbum = [UIAlertAction actionWithTitle:@"我就是想看看相册了……"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                                [self getPictureFromPhotosAlbum];
                                                            }];
        
        [self.actionSheetPictureSource addAction:cancelAction];
        [self.actionSheetPictureSource addAction:photosAlbum];
        
        if ([[self class] isCameraDeviceAvailable])
        {
            UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"我就是要拍一张！"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     [self getPictureFromCamera];
                                                                 }];
            [self.actionSheetPictureSource addAction:cameraAction];
        }
        
        [self.actionSheetPictureSource.view setNeedsLayout];
        
        [ApplicationDelegate.window.rootViewController presentViewController:self.actionSheetPictureSource
                                                                    animated:YES
                                                                  completion:^{
                                                                  }];
    }
}

+ (BOOL)isCameraDeviceAvailable
{
#if TARGET_IPHONE_SIMULATOR
    
    return NO;
    
#endif
    
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]
        || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)pickImageWithType:(UIImagePickerControllerSourceType)sourceType
             andAllowEdit:(BOOL)allowEdit
           andFinishBlock:(void(^)(UIImage *pickImage, NSString *extension))finish
{
    self.pickerAllowEdit = allowEdit;
    self.finishBlock = finish;
    if (self.imagePicker == nil)
    {
        self.imagePicker = [[UIImagePickerController alloc] init];
        self.imagePicker.delegate = self;
    }
    
    self.imagePicker.sourceType = sourceType;
    self.imagePicker.allowsEditing = allowEdit;
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        self.imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
    
    [ApplicationDelegate.window.rootViewController presentViewController:self.imagePicker animated:YES completion:nil];
    
    self.close = NO;
}

- (void)dismiss
{
    [self.actionSheetPictureSource dismissViewControllerAnimated:NO
                                                      completion:nil];
    if (self.imagePicker)
    {
        [self imagePickerControllerDidCancel:self.imagePicker];
    }
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (self.isClose)
    {
        return;
    }
    
    self.close = YES;
    
    UIImage *image;
    
    if (self.imagePicker.allowsEditing)
    {
        image = [info objectForKey:UIImagePickerControllerEditedImage];
    }
    else
    {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
    NSString *extension;
    
    if (self.imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera) //如果是摄像头拍照，则扩展名为JPG
    {
        extension = @"JPG";
    }
    else //其它情况根据文件后缀读取扩展名
    {
        NSURL *assetURL = info[UIImagePickerControllerReferenceURL];
        extension = [assetURL pathExtension];
    }
    
    if (image.size.width > SCREEN_WIDTH) {
        image = [ToolsManager getResizeAndCircleImageWithImage:image
                                                   andMaxWidth:SCREEN_WIDTH - 10.f
                                                  andMaxHeight:image.size.height
                                                   andMinWidth:30.f
                                                  andMinHeight:30.f
                                               andCornerRadius:0.f
                                                andTargetScale:1.f//[UIScreen mainScreen].scale
                                            andBackgroundColor:[UIColor whiteColor]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.finishBlock(image, extension);
    });
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (self.isClose)
    {
        return;
    }
    
    self.close = YES;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.finishBlock(nil, nil);
    });
}

#pragma mark - ImagePicker Get Picture
- (void)getPictureFromCamera
{
    if ([[self class] isCameraDeviceAvailable])
    {
        [self pickImageWithType:UIImagePickerControllerSourceTypeCamera
                   andAllowEdit:self.pickerAllowEdit
                 andFinishBlock:self.finishBlock];
        
        if (IOS_VERSION > 8.0) {
            AVAuthorizationStatus avStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            switch (avStatus) {
                case AVAuthorizationStatusNotDetermined: {//用户尚未决定是否允许访问
                    break;
                }
                case AVAuthorizationStatusRestricted://此应用程序没有被授权访问的照片数据。可能是家长控制权限。
                case AVAuthorizationStatusDenied: {//用户已经明确否认了这一照片数据的应用程序访问.
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"你tm没有允许访问相册……"
                                                                                             message:@"你怎么这么二！！！" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消"
                                                                           style:UIAlertActionStyleCancel
                                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                                             
                                                                         }];
                    
                    UIAlertAction *openAction = [UIAlertAction actionWithTitle:@"快点去打开！二货！！"
                                                                         style:UIAlertActionStyleDestructive
                                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                                       }];
                    
                    [alertController addAction:cancelAction];
                    [alertController addAction:openAction];
                    
                    [alertController.view setNeedsLayout];
                    [ApplicationDelegate.window.rootViewController presentViewController:alertController
                                                                                animated:YES
                                                                              completion:nil];
                    return;
                }
                case AVAuthorizationStatusAuthorized: {//用户已授权应用访问照片数据.
                    break;
                }
            }
        }
    }
}

- (void)getPictureFromPhotosAlbum
{
    [self pickImageWithType:UIImagePickerControllerSourceTypePhotoLibrary
               andAllowEdit:self.pickerAllowEdit
             andFinishBlock:self.finishBlock];
}

@end
