//
//  NotesCommentTableViewCell.h
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesCommentTableViewCell : UITableViewCell

- (void)setCellIndex:(NSInteger)index andAllCellCount:(NSInteger)allCount;

@end
