//
//  NotesCommentTableViewCell.m
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NotesCommentTableViewCell.h"

#import "ToolsManager.h"

@interface NotesCommentTableViewCell ()

@property (strong, nonatomic) UIView *viewSeparatorT;
@property (strong, nonatomic) UIView *viewSeparatorM;
@property (strong, nonatomic) UIView *viewSeparatorB;

@end

@implementation NotesCommentTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.backgroundColor = [UIColor clearColor];
        [self createSeparator];
    }
    
    return self;
}

- (void)createSeparator
{
    //分割线
    {
        self.viewSeparatorT = [[UIView alloc] init];
        self.viewSeparatorT.backgroundColor = Separator_Color;
        [self.contentView addSubview:self.viewSeparatorT];
        
        self.viewSeparatorM = [[UIView alloc] init];
        self.viewSeparatorM.backgroundColor = Separator_Color;
        [self.contentView addSubview:self.viewSeparatorM];
        
        self.viewSeparatorB = [[UIView alloc] init];
        self.viewSeparatorB.backgroundColor = Separator_Color;
        [self.contentView addSubview:self.viewSeparatorB];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.viewSeparatorT.frame = CGRectMake(0, 0, SCREEN_WIDTH, SEPARATOR_HEIGHT);
    self.viewSeparatorM.frame = CGRectMake(15.f, CGRectGetHeight(self.contentView.frame) - SEPARATOR_HEIGHT, SCREEN_WIDTH, SEPARATOR_HEIGHT);
    self.viewSeparatorB.frame = CGRectMake(0, CGRectGetHeight(self.contentView.frame) - SEPARATOR_HEIGHT, SCREEN_WIDTH, SEPARATOR_HEIGHT);
}

- (void)setCellIndex:(NSInteger)index andAllCellCount:(NSInteger)allCount
{
    [self initCellSeparator];
    
    if (index == 0) {
//        [self topCell];
    }
    
    if (index == allCount - 1) {
        [self endCell];
    }
    else{
        [self midCell];
    }
}

#pragma mark -  Separator
- (void)initCellSeparator
{
    self.viewSeparatorT.hidden = YES;
    self.viewSeparatorM.hidden = YES;
    self.viewSeparatorB.hidden = YES;
}

- (void)topCell
{
    self.viewSeparatorT.hidden = NO;
}

- (void)midCell
{
    self.viewSeparatorM.hidden = NO;
}

- (void)endCell
{
    self.viewSeparatorB.hidden = NO;
}

@end
