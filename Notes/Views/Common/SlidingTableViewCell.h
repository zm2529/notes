//
//  SlidingTableViewCell.h
//  Notes
//
//  Created by zm on 2/17/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SlidingTableViewCell;

@protocol SlidingTableViewCellDelegate <NSObject>

@required

@optional
//Will be triggered when tap gesture occured on popping out buttons, this method will show you triggered cell(id) and button's index.
- (void)tapGestureTriggeredOnIndex:(NSInteger)index inCell:(SlidingTableViewCell *)draggedTableViewCell;

@end


@interface SlidingTableViewCell : UITableViewCell

//add main cell view. This view will show to users first.
//Subview's frame would like to be (0, 0, /*Your screen or tableView width*/, cell Height)
- (BOOL)addSubviewAsContent:(UIView *)subview;

//add buttons which will pop out from right of the screen
//buttons will be displayed from left to right, depending on the order you add the subviews to cell
//Subview's frame would like to be (/*Your screen or tableView width*/, 0, /*Your subview's width*/, /*Your subview's height*/)
- (BOOL)addSubviewAsPopOutButton:(UIView *)subview;

- (void)removeSubviewFromContentViewWithIndex:(NSInteger)index;

- (void)resetViewWithAnimation:(BOOL)animation;
- (void)resetViews;
- (void)clickWithIndex:(NSInteger)index;//点击按钮
- (void)cellSeleted;//点击cell

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, weak) id<SlidingTableViewCellDelegate> delegate;

@end
