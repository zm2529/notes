//
//  NoteInformationViewController.h
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NoteInfoData;

@interface NoteInformationViewController : UIViewController

@property (nonatomic, strong) NoteInfoData *noteInfoData;

@end
