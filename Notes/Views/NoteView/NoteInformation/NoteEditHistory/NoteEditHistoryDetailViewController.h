//
//  NoteEditHistoryDetailViewController.h
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NoteEditHistory;

@interface NoteEditHistoryDetailViewController : UIViewController

@property (nonatomic, strong) NoteEditHistory *history;

@end
