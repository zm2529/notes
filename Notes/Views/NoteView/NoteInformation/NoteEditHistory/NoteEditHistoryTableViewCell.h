//
//  NoteEditHistoryTableViewCell.h
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NotesCommentTableViewCell.h"

@class NoteEditHistory;

@interface NoteEditHistoryTableViewCell : NotesCommentTableViewCell

@property (nonatomic, strong) NoteEditHistory *editHistory;

- (void)reloadData;

+ (CGFloat)cellHeight;

@end
