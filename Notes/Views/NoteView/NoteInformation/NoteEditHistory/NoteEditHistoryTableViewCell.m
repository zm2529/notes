//
//  NoteEditHistoryTableViewCell.m
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteEditHistoryTableViewCell.h"

#import "NoteData.h"

#import "ScreenAdaptationHelper.h"
#import "ToolsManager.h"

@interface NoteEditHistoryTableViewCell ()

@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UILabel *lblEditDate;
@property (nonatomic, strong) UILabel *lblContentValidity;
@property (nonatomic, strong) UILabel *lblRecoveryFromDate;

@end

@implementation NoteEditHistoryTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    
    return self;
}

- (void)createUI
{
    self.lblTitle = [[UILabel alloc] init];
    self.lblTitle.text = @" ";
    self.lblTitle.backgroundColor = [UIColor clearColor];
    self.lblTitle.textColor = Orange_Color;
    self.lblTitle.textAlignment = NSTextAlignmentLeft;
    self.lblTitle.font = [UIFont systemFontOfSize:20.f];
    [self.contentView addSubview:self.lblTitle];
    
    self.lblEditDate = [[UILabel alloc] init];
    self.lblEditDate.backgroundColor = [UIColor clearColor];
    self.lblEditDate.textAlignment = NSTextAlignmentLeft;
    self.lblEditDate.font = [UIFont systemFontOfSize:16.f];
    [self.contentView addSubview:self.lblEditDate];
    
    self.lblContentValidity = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 45.f, 0)];
    self.lblContentValidity.backgroundColor = [UIColor clearColor];
    self.lblContentValidity.textAlignment = NSTextAlignmentLeft;
    self.lblContentValidity.font = [UIFont systemFontOfSize:10.f];
    self.lblContentValidity.numberOfLines = 0;
    [self.contentView addSubview:self.lblContentValidity];
    
    self.lblRecoveryFromDate = [[UILabel alloc] init];
    self.lblRecoveryFromDate.backgroundColor = [UIColor clearColor];
    self.lblRecoveryFromDate.textColor = [UIColor redColor];
    self.lblRecoveryFromDate.textAlignment = NSTextAlignmentLeft;
    self.lblRecoveryFromDate.font = [UIFont systemFontOfSize: 8.f];
    [self.contentView addSubview:self.lblRecoveryFromDate];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [ToolsManager fitSizeWithLabel:self.lblTitle];
    [ScreenAdaptationHelper locationLayoutWithAlignment:EnumLocationLayoutAlignmentTopLeft
                                                   view:self.lblTitle
                                                 offset:CGPointMake(15.f, 5.f)
                                          superViewSize:self.contentView.frame.size];
    
    [ToolsManager fitSizeWithLabel:self.lblEditDate];
    [ScreenAdaptationHelper locationLayoutWithAlignment:EnumLocationLayoutAlignmentTopLeft
                                                   view:self.lblEditDate
                                                 offset:CGPointMake(CGRectGetMaxX(self.lblTitle.frame) + 10.f, 8.f)
                                          superViewSize:self.contentView.frame.size];
    
    [ToolsManager fitHeightWithLabel:self.lblContentValidity];
    if (CGRectGetHeight(self.lblContentValidity.frame) > ([[self class] cellHeight] - CGRectGetMaxY(self.lblEditDate.frame) - 15.f)) {
        self.lblContentValidity.frame = (CGRect){
            .size.width = SCREEN_WIDTH - 45.f,
            .size.height = ([[self class] cellHeight] - CGRectGetMaxY(self.lblEditDate.frame) - 15.f),
        };
    }
    [ScreenAdaptationHelper locationLayoutWithAlignment:EnumLocationLayoutAlignmentTopLeft
                                                   view:self.lblContentValidity
                                                 offset:CGPointMake(15.f, CGRectGetMaxY(self.lblEditDate.frame) + 5.f)
                                          superViewSize:self.contentView.frame.size];
    
    if (!self.lblRecoveryFromDate.hidden) {
        [ToolsManager fitSizeWithLabel:self.lblRecoveryFromDate];
        [ScreenAdaptationHelper locationLayoutWithAlignment:EnumLocationLayoutAlignmentLeftBottom
                                                       view:self.lblRecoveryFromDate
                                                     offset:CGPointMake(15.f, -2.f)
                                              superViewSize:self.contentView.frame.size];
    }
}

- (void)reloadData
{
    if (self.editHistory != nil) {
        self.lblTitle.text = self.editHistory.title;
        
        if (self.editHistory.recoveryFromDate == nil || [self.editHistory.recoveryFromDate isEqualToString:@""]) {
            self.lblRecoveryFromDate.hidden = YES;
        }
        else{
            self.lblRecoveryFromDate.hidden = NO;
            self.lblRecoveryFromDate.text = [NSString stringWithFormat:@"恢复自：%@", self.editHistory.recoveryFromDate];
        }
        
        if (self.editHistory.changeTitle == nil) {
            self.lblRecoveryFromDate.hidden = YES;
        }
        else{
            self.lblRecoveryFromDate.hidden = NO;
            self.lblRecoveryFromDate.text = [NSString stringWithFormat:@"修改标题：%@", self.editHistory.changeTitle];
        }
        
        self.lblEditDate.text = self.editHistory.editTime;
        
        self.lblContentValidity.text = self.editHistory.text.string;
        
        [self setNeedsLayout];
    }
}

+ (CGFloat)cellHeight
{
    return 150.f;
}

@end
