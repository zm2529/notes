//
//  NoteEditHistoryDetailViewController.m
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteEditHistoryDetailViewController.h"

#import "ToolsManager.h"
#import "NoteData.h"

@interface NoteEditHistoryDetailViewController ()

@property (nonatomic, strong) UITextView *txtViewHistory;

@end

@implementation NoteEditHistoryDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createUI];
}

- (void)createUI
{
    self.view.backgroundColor = Gray_Color;
    
    UIBarButtonItem *btnrecovery = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemReply
                                    target:self
                                    action:@selector(recovery)];
    self.navigationItem.rightBarButtonItem = btnrecovery;
    
    self.txtViewHistory = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.txtViewHistory.editable = NO;
    self.txtViewHistory.clipsToBounds = NO;
    [self.view addSubview:self.txtViewHistory];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.txtViewHistory.attributedText = self.history.text;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [self setTextViewScrollEnadle];
    
    [self.txtViewHistory setContentOffset:CGPointMake(0, -NAVIGATIONBAR_HEIGHT) animated:YES];
}

- (void)setTextViewScrollEnadle
{
    if (self.txtViewHistory.contentSize.height < (CGRectGetHeight(self.txtViewHistory.frame) - NAVIGATIONBAR_HEIGHT)) {
        self.txtViewHistory.contentSize = CGSizeMake(self.txtViewHistory.contentSize.width, CGRectGetHeight(self.txtViewHistory.frame) + SEPARATOR_HEIGHT - NAVIGATIONBAR_HEIGHT);
    }
}

- (void)recovery
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确定？"
                                                                             message:@"恢复成这个版本……！你懂？！！"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSAttributedString *attString = [[NSAttributedString alloc] initWithString:@""];
        if (self.txtViewHistory.attributedText != nil) {
            attString = [[NSAttributedString alloc] initWithAttributedString:self.txtViewHistory.attributedText];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NOTE_RECOVERY
                                                            object:@{
                                                                     @"noteText" : attString,
                                                                     @"noteDate" : self.history.editTime,
                                                                     @"noteInfoID" : self.history.noteInfoID,
                                                                     }];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [alertController.view setNeedsLayout];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - setter
- (void)setHistory:(NoteEditHistory *)history
{
    _history = history;
    self.title = history.title;
    self.txtViewHistory.attributedText = history.text;
}
@end
