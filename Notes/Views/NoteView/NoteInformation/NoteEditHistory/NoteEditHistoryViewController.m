//
//  NoteEditHistoryViewController.m
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteEditHistoryViewController.h"

#import "AppDelegate.h"

#import "NoteData.h"
#import "ToolsManager.h"

#import "NoteEditHistoryTableViewCell.h"
#import "NoteEditHistoryDetailViewController.h"

@interface NoteEditHistoryViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *noteEditHistoryTableView;

@end

@implementation NoteEditHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createUI];
}

- (void)createUI
{
    self.view.backgroundColor = Gray_Color;
    self.noteEditHistoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                                  0,
                                                                                  SCREEN_WIDTH,
                                                                                  SCREEN_HEIGHT)];
    self.noteEditHistoryTableView.delegate = self;
    self.noteEditHistoryTableView.dataSource = self;
    self.noteEditHistoryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.noteEditHistoryTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.noteEditHistoryTableView reloadData];
    
    self.title = @"修改记录";
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.noteInfoData.arrEditHistory.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *NoteEditHistoryCell = @"NoteEditHistoryCell";
    NoteEditHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NoteEditHistoryCell];
    if (cell == nil) {
        cell = [[NoteEditHistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:NoteEditHistoryCell];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.noteInfoData.arrEditHistory.count) {
        NoteEditHistory *history = self.noteInfoData.arrEditHistory[indexPath.row];

        if ([cell isKindOfClass:[NoteEditHistoryTableViewCell class]]) {
            NoteEditHistoryTableViewCell *historyCell = (NoteEditHistoryTableViewCell *)cell;
            [historyCell setCellIndex:indexPath.row andAllCellCount:self.noteInfoData.arrEditHistory.count];
            historyCell.editHistory = history;
            [historyCell reloadData];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.noteInfoData.arrEditHistory.count) {
        NoteEditHistory *history = self.noteInfoData.arrEditHistory[indexPath.row];
        if (ApplicationDelegate.historyDetailVC == nil) {
            ApplicationDelegate.historyDetailVC = [[NoteEditHistoryDetailViewController alloc] init];
        }
        ApplicationDelegate.historyDetailVC.history = history;
        [self.navigationController pushViewController:ApplicationDelegate.historyDetailVC animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [NoteEditHistoryTableViewCell cellHeight];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TRUE;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end
