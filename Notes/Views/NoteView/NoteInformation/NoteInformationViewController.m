//
//  NoteInformationViewController.m
//  Notes
//
//  Created by zm on 2/16/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteInformationViewController.h"

#import "AppDelegate.h"

#import "ToolsManager.h"
#import "NoteData.h"

#import "NotesCommentTableViewCell.h"
#import "NoteEditHistoryViewController.h"

static NSInteger const Note_Information_Cell_Count = 5;

@interface NoteInformationViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *noteInfoTableView;

@end

@implementation NoteInformationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createUI];
}

- (void)createUI
{
    self.view.backgroundColor = Gray_Color;
    
    self.noteInfoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                                 0,
                                                                                 SCREEN_WIDTH,
                                                                                 SCREEN_HEIGHT)];
    self.noteInfoTableView.delegate = self;
    self.noteInfoTableView.dataSource = self;
    self.noteInfoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.noteInfoTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.noteInfoTableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Note_Information_Cell_Count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *NoteInformationCell = @"NoteInformationCell";
    NotesCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NoteInformationCell];
    if (cell == nil) {
        cell = [[NotesCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:NoteInformationCell];
    }
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    switch (row) {
        case 0:{//标题
            cell.textLabel.text = [NSString stringWithFormat:@"标题：%@", self.noteInfoData.noteTitle];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.userInteractionEnabled = YES;
            break;
        }
        case 1:{//创建时间
            cell.textLabel.text = [NSString stringWithFormat:@"创建时间：%@", self.noteInfoData.createDate];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.userInteractionEnabled = NO;
            break;
        }
        case 2:{//最后修改时间
            cell.textLabel.text = [NSString stringWithFormat:@"最后修改时间：%@", self.noteInfoData.lastEditDate];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.userInteractionEnabled = NO;
            break;
        }
        case 3:{//字数
            cell.textLabel.text = [NSString stringWithFormat:@"字数：%@", self.noteInfoData.wordNumbers];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.userInteractionEnabled = NO;
            break;
        }
        case 4:{//编辑历史
            NSInteger editNumbers = self.noteInfoData.arrEditHistory.count;
            cell.textLabel.text = [NSString stringWithFormat:@"编辑次数：%d", (int)editNumbers];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.userInteractionEnabled = YES;
            break;
        }
        default:
            break;
    }
    
    if ([cell isKindOfClass:[NotesCommentTableViewCell class]]) {
        NotesCommentTableViewCell *noteInfoCell = (NotesCommentTableViewCell *)cell;
        [noteInfoCell setCellIndex:indexPath.row andAllCellCount:Note_Information_Cell_Count];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    switch (row) {
        case 0:{//标题
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"你确定？"
                                                                                     message:@"修改标题"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
                textField.placeholder = self.noteInfoData.noteTitle;
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
                UITextField *txtFolderTitle = alertController.textFields.firstObject;
                if (txtFolderTitle.text != nil) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NOTE_EDIT_TITLE
                                                                        object:@{@"noteTitle" : txtFolderTitle.text,
                                                                                 @"noteInfoID" : self.noteInfoData.noteInfoID,}];
                    
                    [self.noteInfoTableView reloadData];
                }
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [alertController.view setNeedsLayout];
            [self presentViewController:alertController animated:YES completion:nil];
            
            break;
        }
        case 4:{//编辑历史
            
            if (ApplicationDelegate.historyVC == nil) {
                ApplicationDelegate.historyVC = [[NoteEditHistoryViewController alloc] init];
            }
            ApplicationDelegate.historyVC.noteInfoData = self.noteInfoData;
            [self.navigationController pushViewController:ApplicationDelegate.historyVC animated:YES];
            break;
        }
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.f;
}



@end
