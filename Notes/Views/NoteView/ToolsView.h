//
//  ToolsView.h
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ToolsViewDelegate <NSObject>

- (void)insertTime;

- (void)hideKeyboard;

- (void)openSettingView;

- (void)addAPhoto;

- (void)changeFont;

@end

@interface ToolsView : UIView

@property (nonatomic, weak) id<ToolsViewDelegate> delegate;

- (void)beginEditing;
- (void)endEditting;

@end
