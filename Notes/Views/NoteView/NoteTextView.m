//
//  NoteTextView.m
//  Notes
//
//  Created by zm on 2/17/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteTextView.h"

#import "ToolsManager.h"

@implementation NoteTextView

- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated
{
    if (CGRectGetHeight(rect) > 64.f) {
        rect.origin.y -= self.font.lineHeight;
        rect.size.height = 64.f;
    }
    
    [super scrollRectToVisible:rect animated:animated];
}

@end
