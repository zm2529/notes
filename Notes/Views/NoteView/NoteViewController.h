//
//  NoteViewController.h
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NoteData;

@interface NoteViewController : UIViewController

@property (nonatomic, strong) NoteData *note;

@end
