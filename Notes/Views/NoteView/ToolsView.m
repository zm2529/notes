//
//  ToolsView.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "ToolsView.h"

#import "ToolsManager.h"

#import "ScreenAdaptationHelper.h"

@interface ToolsView ()

@property (nonatomic, strong) UIButton *btnSetting;
@property (nonatomic, strong) UIButton *btnInsertTime;
@property (nonatomic, strong) UIButton *btnSetFont;
@property (nonatomic, strong) UIButton *btnAddPhoto;
@property (nonatomic, strong) UIButton *btnEditDone;

@property (nonatomic, strong) NSArray *arrFuncationButton;

@end

@implementation ToolsView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI
{
    self.backgroundColor = White_Color;
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SEPARATOR_HEIGHT)];
    separatorView.backgroundColor = [UIColor colorWithWhite:0.6f alpha:0.2f];
    [self addSubview:separatorView];
    
    //插入时间
    {
        self.btnInsertTime = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80.f, CGRectGetHeight(self.frame))];
        [self.btnInsertTime setTitle:@"时间" forState:UIControlStateNormal];
        [self.btnInsertTime setTitleColor:Orange_Color forState:UIControlStateNormal];
        [self.btnInsertTime addTarget:self action:@selector(btnInsertTimeActionUp) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnInsertTime];
    }
    
    //设置字体
    {
        self.btnSetFont = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80.f, CGRectGetHeight(self.frame))];
        [self.btnSetFont setTitle:@"Aa" forState:UIControlStateNormal];
        [self.btnSetFont setTitleColor:Orange_Color forState:UIControlStateNormal];
        [self.btnSetFont addTarget:self action:@selector(btnSetFontActionUp) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnSetFont];
    }
    
    //设置
    {
        self.btnSetting = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80.f, CGRectGetHeight(self.frame))];
        [self.btnSetting setTitle:@"设置" forState:UIControlStateNormal];
        [self.btnSetting setTitleColor:Orange_Color forState:UIControlStateNormal];
        [self.btnSetting addTarget:self action:@selector(btnSettingActionUp) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnSetting];
    }
    
    //加入照片
    {
        self.btnAddPhoto = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80.f, CGRectGetHeight(self.frame))];
        [self.btnAddPhoto setTitle:@"图片" forState:UIControlStateNormal];
        [self.btnAddPhoto setTitleColor:Orange_Color forState:UIControlStateNormal];
        [self.btnAddPhoto addTarget:self action:@selector(btnAddPhotoActionUp) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnAddPhoto];
    }
    
    //完成
    {
        self.btnEditDone = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80.f, CGRectGetHeight(self.frame))];
        [self.btnEditDone setTitle:@"完成" forState:UIControlStateNormal];
        [self.btnEditDone setTitleColor:Orange_Color forState:UIControlStateNormal];
        [self.btnEditDone addTarget:self action:@selector(btnEditDoneActionUp) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnEditDone];
    }
    
    [self endEditting];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [UIView beginAnimations:@"" context:nil];
    [ScreenAdaptationHelper horizontalAutoSpaceWithViews:self.arrFuncationButton
                             equalSpaceLayoutContentMode:EnumEqualSpaceLayoutContentModeCenter
                                           superViewSize:self.frame.size
                                               leftSpace:0
                                              rightSpace:0];
    [UIView commitAnimations];
}

#pragma mark - Button Action
- (void)btnInsertTimeActionUp
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(insertTime)]) {
        [self.delegate insertTime];
    }
}

- (void)btnSettingActionUp
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(openSettingView)]) {
        [self.delegate openSettingView];
    }
}

- (void)btnEditDoneActionUp
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(hideKeyboard)]) {
        [self.delegate hideKeyboard];
    }
}

- (void)btnSetFontActionUp
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeFont)]) {
        [self.delegate changeFont];
    }
}

- (void)btnAddPhotoActionUp
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addAPhoto)]) {
        [self.delegate addAPhoto];
    }
}

#pragma mark - other
- (void)beginEditing
{
    self.arrFuncationButton = @[self.btnInsertTime,
                                self.btnSetFont,
                                self.btnSetting,
                                self.btnAddPhoto,
                                self.btnEditDone,
                                ];
    
    self.btnSetFont.hidden = NO;
    self.btnEditDone.hidden = NO;
    self.btnSetFont.hidden = NO;
    self.btnAddPhoto.hidden = NO;
    
    [self setNeedsLayout];
}

- (void)endEditting
{
    self.arrFuncationButton = @[
                                self.btnInsertTime,
                                self.btnSetting,
                                ];
    
    self.btnSetFont.hidden = YES;
    self.btnEditDone.hidden = YES;
    self.btnSetFont.hidden = YES;
    self.btnAddPhoto.hidden = YES;
    
    [self setNeedsLayout];
}

@end
