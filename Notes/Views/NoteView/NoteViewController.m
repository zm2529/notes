//
//  NoteViewController.m
//  Notes
//
//  Created by zm on 2/14/16.
//  Copyright © 2016 zm. All rights reserved.
//

#import "NoteViewController.h"

#import "AppDelegate.h"

#import "ToolsManager.h"

#import "NoteData.h"
#import "ToolsView.h"
#import "NoteInformationViewController.h"
#import "ImagePicker.h"
#import "NoteTextView.h"

static const CGFloat ToolsViewHeight = 50.f;

@interface NoteViewController ()<ToolsViewDelegate, UITextViewDelegate>

@property (nonatomic, strong) NoteTextView *txtView;

@property (nonatomic, strong) ToolsView *toolsView;

@property (nonatomic, strong) UIBarButtonItem *btnInfo;
@property (nonatomic, strong) UIBarButtonItem *btnDone;

@property (nonatomic, strong) UIImage *image;

@end

@implementation NoteViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self createUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willKeyboardHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didKeyboardShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didKeyboardHied:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(noteRecovery:)
                                                 name:NOTIFICATION_NOTE_RECOVERY
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(noteEditTitle:)
                                                 name:NOTIFICATION_NOTE_EDIT_TITLE
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackground)
                                                 name:NOTIFICATION_APPLICATION_DID_ENTER_BACKGROUND
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackground)
                                                 name:NOTIFICATION_APPLICATION_WILL_TERMINATE
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackground)
                                                 name:NOTIFICATION_APPLICATION_WILL_RESIGNACTIVE
                                               object:nil];
}

- (void)createUI
{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.btnInfo = [[UIBarButtonItem alloc]
                    initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                    target:self
                    action:@selector(showNoteInfo)];
    
    self.btnDone = [[UIBarButtonItem alloc]
                    initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                    target:self
                    action:@selector(editDone)];
    
    [self.navigationItem setRightBarButtonItems:@[
                                                  self.btnInfo,
                                                  ]];
    
    
    //contentView
    {
        
        self.txtView = [[NoteTextView alloc] initWithFrame:CGRectMake(0,
                                                                    0,
                                                                    SCREEN_WIDTH,
                                                                    SCREEN_HEIGHT - ToolsViewHeight)];
        self.txtView.clipsToBounds = NO;
        self.txtView.delegate = self;
        [self.view addSubview:self.txtView];
        
        self.toolsView = [[ToolsView alloc] initWithFrame:CGRectMake(0,
                                                                     0,
                                                                     SCREEN_WIDTH,
                                                                     ToolsViewHeight)];
        self.toolsView.delegate = self;
        [self.view addSubview:self.toolsView];
        
        [self changeTextViewHeight:SCREEN_HEIGHT - 0];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    self.txtView.attributedText = self.note.text;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self setTextViewScrollEnadle];
    
    [self.txtView setContentOffset:CGPointMake(0, -NAVIGATIONBAR_HEIGHT) animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
//    [self editDone];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self editDone];
}

- (void)setTextViewScrollEnadle
{
    if (self.txtView.contentSize.height < (CGRectGetHeight(self.txtView.frame) - NAVIGATIONBAR_HEIGHT)) {
        self.txtView.contentSize = CGSizeMake(self.txtView.contentSize.width, CGRectGetHeight(self.txtView.frame) + SEPARATOR_HEIGHT - NAVIGATIONBAR_HEIGHT);
    }
}

- (void)showNoteInfo
{
    [self editDone];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"其他东西"
                                                                             message:@""
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    UIAlertAction *backupAction = [UIAlertAction actionWithTitle:@"备份"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                         }];
    UIAlertAction *noteInfoAction =
    [UIAlertAction actionWithTitle:@"信息"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action) {
                               
                               if (ApplicationDelegate.noteInformationVC == nil) {
                                   ApplicationDelegate.noteInformationVC = [[NoteInformationViewController alloc] init];
                               }
                               ApplicationDelegate.noteInformationVC.noteInfoData = self.note.noteInfo;
                               [self.navigationController pushViewController:ApplicationDelegate.noteInformationVC
                                                                    animated:YES];
                           }];
    [alertController addAction:cancelAction];
    [alertController addAction:backupAction];
    [alertController addAction:noteInfoAction];
    
    [alertController.view setNeedsLayout];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)editDone
{
//    if (self.txtView.isFirstResponder) {
    [self.txtView resignFirstResponder];
    
    [self.note reloadWithNote:self.txtView.attributedText];
//    }
}

- (void)changeTextViewHeight:(CGFloat)height
{
    self.txtView.frame = CGRectMake(0,
                                    0,
                                    SCREEN_WIDTH,
                                    height - ToolsViewHeight);
    
    self.toolsView.frame = CGRectMake(0,
                                      height - ToolsViewHeight + 0,
                                      SCREEN_WIDTH,
                                      ToolsViewHeight);
}

#pragma mark - Notification
- (void)willKeyboardShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    
    // Get the size of the keyboard.
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIViewAnimationCurve animationCurve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    //设置动画
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:animationCurve];
    [UIView setAnimationDuration:animationDuration];
    
    [self changeTextViewHeight:SCREEN_HEIGHT - keyboardSize.height - 0];
    
    [UIView commitAnimations];
}

- (void)willKeyboardHide:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIViewAnimationCurve animationCurve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    //设置动画
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:animationCurve];
    [UIView setAnimationDuration:animationDuration];
    
    [self changeTextViewHeight:SCREEN_HEIGHT - 0];
    
    [UIView commitAnimations];
}

- (void)didKeyboardShow:(NSNotification *)notification
{
    [self setTextViewScrollEnadle];
}

- (void)didKeyboardHied:(NSNotification *)notification
{
    [self setTextViewScrollEnadle];
}

- (void)noteRecovery:(NSNotification *)notification
{
    NSDictionary *dicNotificationObject = notification.object;
    NSString *noteInfoID = [dicNotificationObject objectForKey:@"noteInfoID"];
    if (![noteInfoID isEqualToString:self.note.noteInfo.noteInfoID]) {
        return;
    }
    NSAttributedString *attString = [dicNotificationObject objectForKey:@"noteText"];
    NSString *date = [dicNotificationObject objectForKey:@"noteDate"];
    
    [self.note recoveryWithNote:attString andFromDate:date];
    self.txtView.attributedText = attString;
    
    [self.navigationController popToViewController:self animated:YES];
}

- (void)noteEditTitle:(NSNotification *)notification
{
    NSDictionary *dicNotificationObject = notification.object;
    NSString *noteInfoID = [dicNotificationObject objectForKey:@"noteInfoID"];
    if (![noteInfoID isEqualToString:self.note.noteInfo.noteInfoID]) {
        return;
    }
    NSString *title = [dicNotificationObject objectForKey:@"noteTitle"];
    
    [self.note updateTitle:title];
    self.title = title;
}

- (void)appDidEnterBackground
{
    [self editDone];
}

#pragma mark - setter
- (void)setNote:(NoteData *)note
{
    _note = note;
    self.title = note.noteInfo.noteTitle;
    self.txtView.attributedText = note.text;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.toolsView beginEditing];
    
    [self.navigationItem setRightBarButtonItems:@[
                                                  self.btnDone,
                                                  self.btnInfo,
                                                  ]
                                       animated:YES];
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self.toolsView endEditting];
    
    [self.navigationItem setRightBarButtonItems:@[
                                                  self.btnInfo,
                                                  ]
                                       animated:YES];
}

#pragma  mark - ToolsViewDelegate
- (void)insertTime
{
    NSString *strDate = [ToolsManager timeStringOfNowWithFormat:@"yyyy年MM月dd日  HH:mm:ss"];
    NSRange selectRange = self.txtView.selectedRange;
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithAttributedString:self.txtView.attributedText];
    
    [attString replaceCharactersInRange:selectRange withAttributedString:[[NSAttributedString alloc] initWithString:strDate]];
    
    self.txtView.attributedText = [attString copy];
    
    self.txtView.selectedRange = NSMakeRange(selectRange.location + strDate.length, 0);
    
    [self.txtView scrollRangeToVisible:self.txtView.selectedRange];
}

- (void)openSettingView
{

}

- (void)hideKeyboard
{
    [self editDone];
}

- (void)changeFont
{

}

- (void)addAPhoto
{
    ImagePicker *imagePicker = [[ImagePicker alloc] init];
    [imagePicker displayOperateActionSheetWithAllowEdit:NO
                                         andFinishBlock:^(UIImage *pickImage, NSString *extension) {
                                             if (pickImage != nil) {
                                                 [self performSelector:@selector(insertImage:)
                                                            withObject:pickImage
                                                            afterDelay:0];
                                             }
                                         }];
}

- (void)insertImage:(UIImage *)image
{
    if (image == nil) {
        return;
    }
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = image;
    textAttachment.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
    
    NSRange selectRange = self.txtView.selectedRange;
    NSAttributedString *attStringAttachment = [NSAttributedString attributedStringWithAttachment:textAttachment];
    
    NSMutableAttributedString *attMutString = [[NSMutableAttributedString alloc] initWithAttributedString:self.txtView.attributedText];
    [attMutString replaceCharactersInRange:selectRange withString:@"\n"];
    [attMutString replaceCharactersInRange:self.txtView.selectedRange
                      withAttributedString:attStringAttachment];
    
    self.txtView.attributedText = [attMutString copy];
    self.txtView.selectedRange = NSMakeRange(selectRange.location + attStringAttachment.length + 1, 0);
    
    [self.txtView becomeFirstResponder];
}
@end
